﻿using SmartGarage.Data.Models;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.CreateDTOs;
using System;

namespace SmartGarage.Services.ModelMapper
{
    public class ModelMapper : IModelMapper
    {
        public Manufacturer ToManufacturer(ManufacturerDTO manufacturerDTO)
        {
            if (manufacturerDTO == null)
                throw new ArgumentException();

            var manufacturer = new Manufacturer()
            {
                Name = manufacturerDTO.Name
            };

            return manufacturer;
        }

        public VehicleModel ToVehicleModel(VehicleModelDTO vehicleModelDTO)
        {
            if (vehicleModelDTO == null)
                throw new ArgumentException();

            var vehicleModel = new VehicleModel()
            {
                Name = vehicleModelDTO.Name,
                ManufacturerId = vehicleModelDTO.ManufacturerId
            };

            return vehicleModel;
        }
        public VehicleModel ToVehicleModelWithId(VehicleModelDTO vehicleModelDTO)
        {
            if (vehicleModelDTO == null)
                throw new ArgumentException();

            var vehicleModel = new VehicleModel()
            {
                Id = vehicleModelDTO.Id,
                Name = vehicleModelDTO.Name,
                ManufacturerId = vehicleModelDTO.ManufacturerId
            };

            return vehicleModel;
        }


        public Vehicle ToVehicle(CreateVehicleDTO vehicleDTO)
        {
            if (vehicleDTO == null)
            {
                throw new ArgumentException();
            }

            var vehicle = new Vehicle
            {
                ModelId = vehicleDTO.ModelId,
                CustomerId = vehicleDTO.CustomerId,
                Type = (Data.Models.Enums.VehicleTypes)vehicleDTO.Type,
                IdentityNumber = vehicleDTO.IdentityNumber,
                PlateNumber = vehicleDTO.PlateNumber
            };

            return vehicle;
        }

        public Visit ToVisit(CreateVisitDTO visitDTO)
        {
            if (visitDTO == null)
            {
                throw new ArgumentException();
            }

            var visit = new Visit
            {
                VehicleId = visitDTO.VehicleId,
                DateIn = DateTime.Now,
                Status = (Data.Models.Enums.Status)2
            };

            return visit;
        }

        public Service ToService(ServiceDTO serviceDTO)
        {
            if (serviceDTO == null)
            {
                throw new ArgumentException();
            }

            var service = new Service
            {
                Name = serviceDTO.Name,
                Price = serviceDTO.Price,
                Description = serviceDTO.Description
            };

            return service;
        }

        public User ToCustomer(CreateUserDTO userDTO)
        {
            if (userDTO == null)
            {
                throw new ArgumentException();
            }

            var user = new User
            {
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName,
                PhoneNumber = userDTO.Phone,
                Email = userDTO.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                NormalizedEmail = userDTO.Email.ToUpper(),
                UserName = userDTO.Email,
                NormalizedUserName = userDTO.Email.ToUpper()
            };

            return user;
        }
    }
}