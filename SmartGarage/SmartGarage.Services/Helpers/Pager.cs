﻿using SmartGarage.Services.QueryObject;
using System;
using System.Collections.Generic;

namespace SmartGarage.Services.Helpers
{
    public class Pager<T>
    {
        public Pager(List<T> items, PagerQueryObject pagerQuery, int totalCount)
        {
            this.ItemsOnPage = items.Count;
            this.Page = pagerQuery.PageNumber;
            this.Items = items;
            this.Count = totalCount;
            this.TotalPages = (int)Math.Ceiling(totalCount / (double)pagerQuery.ItemsOnPage);
        }

        public int Count { get; set; }
        public int Page { get; set; }
        public int ItemsOnPage { get; set; }
        public List<T> Items { get; set; }
        public int TotalPages { get; set; }

        public bool PrevPage
        {
            get
            {
                return (Page > 1);
            }
        }

        public bool NextPage
        {
            get
            {
                return (Page < TotalPages);
            }
        }
    }
}