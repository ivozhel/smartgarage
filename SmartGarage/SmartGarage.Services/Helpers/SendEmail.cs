﻿using SmartGarage.Services.Models;
using Syncfusion.Drawing;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Grid;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Helpers
{
    public static class SendEmail
    {
        public static void SendPassword(string password, string email)
        {
            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential("smarggaragetest@gmail.com", "smartgarage123"),
                EnableSsl = true,
            };
            var mail = new MailMessage()
            {
                IsBodyHtml = true,
                From = new MailAddress("smarggaragetest@gmail.com"),
                Subject = "Password",
                Body = $"Your password is: {password}"
            };
            mail.To.Add(email);
            smtpClient.Send(mail);
        }
        public static async Task SendPdfAsync(List<VisitDTO> visits, string email)
        {
            var sb = new StringBuilder();

            foreach (var item in visits)
            {
                sb.AppendLine($"Visit Details");
                sb.AppendLine();
                sb.AppendLine($"Customer Name: {item.CustomerName}");
                sb.AppendLine();
                item.Services.ForEach(s => sb.AppendLine($"{s.Name}: {s.Price} EUR"));
                var totalPrice = item.Services.Sum(s => s.Price);
                sb.AppendLine();
                sb.AppendLine($"Total Price:{totalPrice} EUR");
                sb.AppendLine();

            }   

            PdfDocument doc = new PdfDocument();

            PdfPage page = doc.Pages.Add();

            PdfGraphics g = page.Graphics;
            // Create a solid brush
            PdfBrush brush = new PdfSolidBrush(Color.Black);
            // Set the font.
            PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 20f);
            // Draw the text.
            g.DrawString(sb.ToString(), font, brush, new PointF(20, 20));
            
            //PdfGrid pdfGrid = new PdfGrid();

            //pdfGrid.DataSource = visits;
            //pdfGrid.Draw(page, new Syncfusion.Drawing.PointF(10, 10));

            MemoryStream stream = new MemoryStream();

            doc.Save(stream);

            doc.Close(true);


            stream.Position = 0;

            Attachment file = new Attachment(stream, "Attachment.pdf", "application/pdf");

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("smarggaragetest@gmail.com");
                mail.To.Add(email);
                mail.Subject = "Generated PDF report for visit/s.";
                mail.Body = "PDF";
                mail.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                {
                    smtp.Credentials = new NetworkCredential("smarggaragetest@gmail.com", "smartgarage123");
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.EnableSsl = true;
                    mail.Attachments.Add(new Attachment(stream, "VisitDetails.pdf"));
                    await smtp.SendMailAsync(mail);
                }
            }
        }
    }
}