﻿using Newtonsoft.Json;

namespace SmartGarage.Services.Extensions.CurrencyExtensions
{
    public class Currency
    {
        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("rates")]
        public Rates Rates { get; set; }
    }

    public class Rates
    {
        [JsonProperty("USD")]
        public decimal USD { get; set; }

        [JsonProperty("BGN")]
        public decimal BGN { get; set; }

        [JsonProperty("GBP")]
        public decimal GBP { get; set; }
    }
}