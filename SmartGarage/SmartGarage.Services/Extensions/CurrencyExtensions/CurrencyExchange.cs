﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SmartGarage.Services.Extensions.CurrencyExtensions
{
    public static class CurrencyExchange
    {
        private static readonly string key = "2011ea3b4a2910a2c73d526a1fe5c334";
        private static readonly string baseCurrency = "EUR";
        private static readonly HttpClient client = new HttpClient();

        public static async Task<decimal> ExchangeCurrency(DateTime date, string currency)
        {
            var dateInFormat = date.ToString("yyyy-MM-dd");
            if (currency == baseCurrency)
            {
                return 1;
            }
            var rates = new Currency();
            using (var response = await client.GetAsync($"http://data.fixer.io/api/{dateInFormat}?access_key={key}&base={baseCurrency}&symbols=USD,GBP,BGN"))
            {
                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();

                    rates = JsonConvert.DeserializeObject<Currency>(result);
                }
                else
                {
                    throw new ArgumentException("Currency exchange fail");
                }
            }

            switch (currency)
            {
                case "BGN":
                    return Math.Round(rates.Rates.BGN, 2);

                case "USD":
                    return Math.Round(rates.Rates.USD, 2);

                case "GBP":
                    return Math.Round(rates.Rates.GBP, 2);

                default:
                    throw new ArgumentException("Currency is not valid");
            }
        }
    }
}