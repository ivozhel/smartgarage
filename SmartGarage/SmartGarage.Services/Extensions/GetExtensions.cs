﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Extensions
{
    //TODO: kill it pls
    public static class GetExtensions
    {
        //Vehicle Extensions
        public static async Task<Vehicle> GetSingleVehicle(SmartGarageContext context, int? id)
        {
            var vehicle = await context.Vehicles
             .Include(v => v.Customer)
             .Include(v => v.Visits)
             .Include(v => v.Model)
                 .ThenInclude(m => m.Manufacturer)
             .FirstOrDefaultAsync(x => x.Id == id);

            return vehicle;
        }

        public static IQueryable<Vehicle> GetAllVehicles(SmartGarageContext context)
        {
            var vehicles = context.Vehicles
                .Where(x => !x.IsDeleted)
                 .Include(v => v.Customer)
                 .Include(v => v.Visits)
                 .Include(v => v.Model)
                    .ThenInclude(m => m.Manufacturer)
                    .AsQueryable().AsNoTracking();

            return vehicles;
        }

        //Visit Extensions
        public static async Task<IQueryable<Visit>> GetAllVisits(SmartGarageContext context)
        {
            var visits = await Task.FromResult(context.Visits
                .Include(v => v.Vehicle)
                    .ThenInclude(c => c.Customer));

            return visits;
        }

        public static async Task<Visit> GetSingleVisit(SmartGarageContext context, int id)
        {
            var visit = await context.Visits
             .Include(v => v.Vehicle)
                    .ThenInclude(c => c.Customer)
             .FirstOrDefaultAsync(x => x.Id == id);

            return visit;
        }
    }
}