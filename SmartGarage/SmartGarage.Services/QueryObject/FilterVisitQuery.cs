﻿using System;

namespace SmartGarage.Services.QueryObject
{
    public class FilterVisitQuery
    {
        public string Status { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PlateNumber { get; set; }
        public string IdentityNumber { get; set; }
        public string Vehicle { get; set; }
        public bool DateFilter { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
