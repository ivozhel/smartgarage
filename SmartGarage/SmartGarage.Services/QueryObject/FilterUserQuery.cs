﻿using System;

namespace SmartGarage.Services.QueryObject
{
    public class FilterUserQuery
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Vehicle { get; set; }      

        public DateTime From { get; set; }
        public DateTime To { get; set; }

        //public string Order { get; set; }
        
        //true = sort ascending
        //filter date out or in(true = in , false = out)
        public bool DateFilter { get; set; }
        public string Order { get; set; }
        public string SortBy { get; set; }
        //public bool SortByName { get; set; }
        //public bool SortByVisitDate { get; set; }
    }
}