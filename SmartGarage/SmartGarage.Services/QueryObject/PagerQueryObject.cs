﻿namespace SmartGarage.Services.QueryObject
{
    public class PagerQueryObject
    {
        public PagerQueryObject(int pageNumber, int itemsOnPage)
        {
            this.PageNumber = pageNumber == 0 ? 1 : pageNumber;

            this.ItemsOnPage = itemsOnPage == 0 ? 5 : itemsOnPage;
        }

        public int PageNumber { get; set; }
        public int ItemsOnPage { get; set; }
    }
}