﻿namespace SmartGarage.Services.QueryObject
{
    public class FilterServiceQuery
    {
        public string Name { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }

    }
}