﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Helpers;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class SvcService : ISvcService
    {
        private readonly IModelMapper modelMapper;
        private readonly SmartGarageContext context;

        public SvcService(SmartGarageContext context, IModelMapper modelMapper)
        {
            this.context = context;
            this.modelMapper = modelMapper;
        }

        public async Task<bool> CreateAsync(ServiceDTO model)
        {
            var serviceToCreate = modelMapper.ToService(model);
            if (serviceToCreate == null)
            {
                return false;
            }

            await context.Services.AddAsync(serviceToCreate);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<ServiceDTO> GetAsync(int id)
        {
            var service = await context.Services.FirstOrDefaultAsync(s => s.Id == id);

            if (service == null)
            {
                return null;
            }

            return new ServiceDTO(service);
        }

        public async Task<Pager<ServiceDTO>> GetAllAsync(FilterServiceQuery serviceQuery, PagerQueryObject pagerQuery)
        {
            IQueryable<Service> services = context.Services.OrderByDescending(s => s.Id);

            var skip = (pagerQuery.PageNumber - 1) * pagerQuery.ItemsOnPage;


            if (serviceQuery.Name != null)
            {
                services = services.Where(s => s.Name.Contains(serviceQuery.Name));
            }

            if (serviceQuery.PriceFrom != default)
            {
                services = services.Where(s => (s.Price >= serviceQuery.PriceFrom));
            }
            if (serviceQuery.PriceTo != default)
            {
                services = services.Where(s => s.Price <= serviceQuery.PriceTo);
            }

            var servicesDTOs = await services.Skip(skip)
                .Take(pagerQuery.ItemsOnPage)
                .Select(m => new ServiceDTO(m))
                .ToListAsync();

            Pager<ServiceDTO> result = new Pager<ServiceDTO>(servicesDTOs, pagerQuery, services.Count());

            return result;
        }

        public async Task<IEnumerable<ServiceDTO>> GetAllAsync()
        {
            //if there are no services make check
            return await context.Services.Select(m => new ServiceDTO(m)).ToListAsync();
        }

        public async Task<bool> UpdateAsync(int id, ServiceDTO model)
        {
            var service = await context.Services.FirstOrDefaultAsync(s => s.Id == id);

            if (service == null || model == null)
            {
                //TODO: throw custom exception not found
                return false;
            }

            if (service.Price != model.Price)
            {
                model.Name = model.Name ?? service.Name;
                model.Description = model.Description ?? service.Description;
                await this.CreateAsync(model);
                await this.DeleteAsync(service.Id);
            }
            else
            {
                service.Name = model.Name ?? service.Name;
                service.Description = model.Description ?? service.Description;
                context.Services.Update(service);
                await context.SaveChangesAsync();
            }

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var service = await context.Services.FirstOrDefaultAsync(s => s.Id == id);
            //var visit = await context.Visits.FirstOrDefaultAsync(v => v.Id == id);

            if (service == null)
            {
                return false;
            }

            context.Services.Remove(service);
            await context.SaveChangesAsync();

            return true;
        }
    }
}