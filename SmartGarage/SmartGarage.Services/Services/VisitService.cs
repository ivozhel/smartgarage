﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.CustomExceptions.CustomExceptions;
using SmartGarage.Data;
using SmartGarage.Data.CustomAttributes;
using SmartGarage.Data.Models;
using SmartGarage.Services.Extensions;
using SmartGarage.Services.Helpers;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class VisitService : IVisitService
    {
        private readonly IModelMapper modelMapper;
        private readonly SmartGarageContext context;

        public VisitService(SmartGarageContext context, IModelMapper modelMapper)
        {
            this.context = context;
            this.modelMapper = modelMapper;
        }
        public async Task<int> Create(CreateVisitDTO model)
        {
            var visit = modelMapper.ToVisit(model);
            var customer = context.Users.FirstOrDefault(x => x.Vehicles.Any(v => v.Id == model.VehicleId));
            if (visit == null)
            {
                return 0;
            }
            customer.LastVisit = DateTime.Now;
            context.Users.Update(customer);
            await context.Visits.AddAsync(visit);
            await context.SaveChangesAsync();

            return visit.Id;

        }
        public async Task<bool> CreateAsync(CreateVisitDTO model)
        {
            var visit = modelMapper.ToVisit(model);
            var customer = context.Users.FirstOrDefault(x => x.Vehicles.Any(v => v.Id == model.VehicleId));

            if (visit == null)
            {
                return false;
            }

            customer.LastVisit = DateTime.Now;

            await context.Visits.AddAsync(visit);
            context.Users.Update(customer);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<VisitDTO> GetAsync(int id, string currency = "EUR")
        {
            var visit = await context.Visits
                .Include(v => v.Vehicle)
                    .ThenInclude(c => c.Customer)
                    .Include(v => v.Vehicle.Model.Manufacturer)
                .Include(v => v.Services).IgnoreQueryFilters()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (visit == null)
            {
                return null;
            }

            return new VisitDTO(visit, currency);
        }

        public async Task<Pager<VisitDTO>> GetAllAsync(FilterVisitQuery visitQuery, PagerQueryObject pagerQuery, string currency, string isCheckedOut = "All", int customerId = 0)
        {
            var skip = (pagerQuery.PageNumber - 1) * pagerQuery.ItemsOnPage;
            IQueryable<Visit> visits;
            switch (isCheckedOut)
            {
                case "CheckedOut":
                    visits = context.Visits.Include(v => v.Services).IgnoreQueryFilters()
                    .Include(v => v.Vehicle)
                    .ThenInclude(ve => ve.Customer).Include(v => v.Vehicle.Model.Manufacturer).Where(x => x.DateOut != null);
                    break;
                case "NotCheckedOut":
                    visits = context.Visits.Include(v => v.Services).IgnoreQueryFilters()
                     .Include(v => v.Vehicle)
                        .ThenInclude(ve => ve.Customer).Include(v => v.Vehicle.Model.Manufacturer).Where(x => x.DateOut == null);
                    break;
                default:
                    visits = context.Visits.Include(v => v.Services).IgnoreQueryFilters()
                    .Include(v => v.Vehicle)
                    .ThenInclude(ve => ve.Customer).Include(v => v.Vehicle.Model.Manufacturer);
                    break;
            }

            visits = visits.OrderByDescending(s => s.Id);

            if (customerId!=0)
            {
                visits = visits.Where(x => x.Vehicle.CustomerId == customerId);
            }

            if (visitQuery.Status != null)
            {
                var input = StatusParser.Parse(visitQuery.Status);
                visits = visits.Where(v => v.Status == input);
            }

            if (visitQuery.FirstName != null)
            {
                visits = visits.Where(v => v.Vehicle.Customer.FirstName == visitQuery.FirstName);
            }

            if (visitQuery.LastName != null)
            {
                visits = visits.Where(v => v.Vehicle.Customer.LastName == visitQuery.LastName);
            }

            if (visitQuery.PlateNumber != null)
            {
                visits = visits.Where(v => v.Vehicle.PlateNumber == visitQuery.PlateNumber);
            }

            if (visitQuery.IdentityNumber != null)
            {
                visits = visits.Where(v => v.Vehicle.IdentityNumber == visitQuery.IdentityNumber);
            }

            if (visitQuery.Vehicle != null)
            {
                visits = visits.Where(v => v.Vehicle.Model.Name.Contains(visitQuery.Vehicle) || v.Vehicle.Model.Manufacturer.Name.Contains(visitQuery.Vehicle)
                || v.Vehicle.PlateNumber.Contains(visitQuery.Vehicle));
            }

            if (visitQuery.DateFrom != default || visitQuery.DateTo != default)
            {
                if (visitQuery.DateFilter)
                {
                    if (visitQuery.DateFrom != default && visitQuery.DateTo != default)
                    {
                        visits = visits.Where(v => v.DateIn >= visitQuery.DateFrom && v.DateIn <= visitQuery.DateTo);
                    }
                    else if (visitQuery.DateFrom != default)
                    {
                        visits = visits.Where(v => v.DateIn >= visitQuery.DateFrom);
                    }
                    else
                    {
                        visits = visits.Where(v => v.DateIn <= visitQuery.DateTo);
                    }
                }
                else
                {
                    if (visitQuery.DateFrom != default && visitQuery.DateTo != default)
                    {
                        visits = visits.Where(v => v.DateOut >= visitQuery.DateFrom && v.DateOut <= visitQuery.DateTo);
                    }
                    else if (visitQuery.DateFrom != default)
                    {
                        visits = visits.Where(v => v.DateOut >= visitQuery.DateFrom);
                    }
                    else
                    {
                        visits = visits.Where(v => v.DateOut <= visitQuery.DateTo);
                    }

                }

            }
            var visitsDTOs = await visits.Skip(skip)
               .Take(pagerQuery.ItemsOnPage)
               .Select(m => new VisitDTO(m, currency))
               .ToListAsync();

            Pager<VisitDTO> result = new Pager<VisitDTO>(visitsDTOs, pagerQuery, visits.Count());

            return result;
        }

        public async Task<IEnumerable<VisitDTO>> GetAllAsync(int id = 0)
        {
            if (id != 0)
            {

                var result = await context.Visits
                    .Include(v => v.Services)
                    .Include(v => v.Vehicle)
                    .ThenInclude(ve => ve.Customer).Include(v => v.Vehicle.Model.Manufacturer)
                    .Select(v => new VisitDTO(v, null)).ToListAsync();

                return result.Where(x => x.CustomerId == id).ToList();

            }

            return await context.Visits
                .Include(v => v.Services)
                .Include(v => v.Vehicle)
                .ThenInclude(ve => ve.Customer).Include(v => v.Vehicle.Model.Manufacturer)
                .Select(v => new VisitDTO(v, null)).ToListAsync();
        }


        public async Task<bool> UpdateAsync(int id, UpdateVisitDTO model)
        {
            var visit = context.Visits.FirstOrDefault(v => v.Id == id);

            if (visit == null || model == null)
            {
                return false;
            }

            visit.Status = (Data.Models.Enums.Status)model.Status;

            context.Visits.Update(visit);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var visit = await GetExtensions.GetSingleVisit(context, id);

            if (visit == null)
            {
                return false;
            }

            context.Visits.Remove(visit);
            await context.SaveChangesAsync();

            return true;
        }
        public async Task<bool> AddServiceToVisit(int visitId, int serviceId)
        {
            //TODO: throw Ex
            var visit = await context.Visits.FirstOrDefaultAsync(v => v.Id == visitId);
            var service = await context.Services.FirstOrDefaultAsync(s => s.Id == serviceId);

            if (service == null)
            {
                return false;
            }

            if (visit.Services == null)
            {
                visit.Services = new List<Service>() { service };
            }
            else
            {
                visit.Services.Add(service);
            }


            await context.SaveChangesAsync();

            return true;
        }
        public async Task<bool> DeleteServiceFromVisit(int visitId, int serviceId)
        {
            //TODO: throw Ex
            var visit = await context.Visits.Include(s => s.Services).FirstOrDefaultAsync(v => v.Id == visitId);
            //var service = await context.Services.Include(s => s.Visits).FirstOrDefaultAsync(s => s.Id == serviceId);

            if (!context.Services.Any(x => x.Id == serviceId))
            {
                return false;
            }

            if (visit.Services.Any(x => x.Id == serviceId))
            {
                var serviceToDel = visit.Services.SingleOrDefault(x => x.Id == serviceId);
                visit.Services.Remove(serviceToDel);
            }

            await context.SaveChangesAsync(true);

            return true;
        }

        public async Task<bool> CheckOutVisit(int id)
        {
            var visit = await GetExtensions.GetSingleVisit(context, id);

            if (!(visit.Status == 0))
            {
                return false;
            }

            visit.DateOut = DateTime.Now;
            context.Visits.Update(visit);
            await context.SaveChangesAsync();

            return true;

        }
        public async Task<bool> ChangeStatus(int id, int status)
        {
            var visit = context.Visits.Include(v => v.Vehicle.Customer).
                Include(v => v.Vehicle.Model.Manufacturer)
                .Include(v => v.Services).FirstOrDefault(x => x.Id == id);
            if (visit == null)
            {
                return false;
            }
            visit.Status = (Data.Models.Enums.Status)status;

            context.Visits.Update(visit);
            await context.SaveChangesAsync();
            if (status == 0)
            {
                var pdfList = new List<VisitDTO>() { new VisitDTO(visit, "EUR") };
                await SendEmail.SendPdfAsync(pdfList, visit.Vehicle.Customer.Email);
            }
            return true;
        }
    }
}