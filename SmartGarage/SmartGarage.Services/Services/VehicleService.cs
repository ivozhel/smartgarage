﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Extensions;
using SmartGarage.Services.Helpers;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly SmartGarageContext context;
        private readonly IModelMapper modelMapper;

        public VehicleService(SmartGarageContext context, IModelMapper modelMapper)
        {
            this.context = context;
            this.modelMapper = modelMapper;
        }

        public async Task<VehicleDTO> GetAsync(int? id)
        {
            var vehicle = await GetExtensions.GetSingleVehicle(context, id);
            if (vehicle == null)
            {
                return null;
            }
            return new VehicleDTO(vehicle);
        }

        public async Task<Pager<VehicleDTO>> GetAllAsync(string customerName, PagerQueryObject pagerQuery, int customerId = 0)
        {
            var vehicles = GetExtensions.GetAllVehicles(context).OrderByDescending(s => s.Id);
            var queryResult = new List<Vehicle>();
            var vehicleDTOs = new List<VehicleDTO>();
            var skip = (pagerQuery.PageNumber - 1) * pagerQuery.ItemsOnPage;
            int countForPager;
            if (customerId != 0)
            {
                queryResult = vehicles
                    .Where(v => v.CustomerId == customerId)
                    .ToList();
                countForPager = queryResult.Count();

                queryResult = queryResult.Skip(skip)
                .Take(pagerQuery.ItemsOnPage).ToList();
            }
            else if (string.IsNullOrEmpty(customerName))
            {
                countForPager = vehicles.Where(x => !x.IsDeleted).Count();
                queryResult = vehicles.OrderBy(x => x.Id).Skip(skip)
                .Take(pagerQuery.ItemsOnPage).ToList();
            }
            else
            {
                queryResult = vehicles
                    .Where(v => (v.Customer.FirstName + " " + v.Customer.LastName).Contains(customerName))
                    .ToList();
                countForPager = queryResult.Count();

                queryResult = queryResult.Skip(skip)
                .Take(pagerQuery.ItemsOnPage).ToList();
            }

            foreach (var item in queryResult)
            {
                vehicleDTOs.Add(new VehicleDTO(item));
            }

            Pager<VehicleDTO> result = new Pager<VehicleDTO>(vehicleDTOs, pagerQuery, countForPager);

            return result;
        }

        public async Task<bool> CreateAsync(CreateVehicleDTO model)
        {
            var vehicle = modelMapper.ToVehicle(model);

            await context.Vehicles.AddAsync(vehicle);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateAsync(UpdateVehicleDTO model, int id)
        {
            var vehicle = await GetExtensions.GetSingleVehicle(context, id);
            if (vehicle == null || model == null)
            {
                return false;
            }

            if (model.CustomerId != default)
            {
                vehicle.CustomerId = (int)model.CustomerId;
            }
            if (model.ModelId != default)
            {
                vehicle.ModelId = (int)model.ModelId;
            }

            vehicle.PlateNumber = model.PlateNumber ?? vehicle.PlateNumber;

            vehicle.IdentityNumber = model.IdentityNumber ?? vehicle.IdentityNumber;

            if (model.Type != default)
            {
                vehicle.Type = (Data.Models.Enums.VehicleTypes)model.Type;
            }

            context.Vehicles.Update(vehicle);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var vehicle = await context.Vehicles.FirstOrDefaultAsync(v => v.Id == id);
            if (vehicle == null)
            {
                return false;
            }
            context.Vehicles.Remove(vehicle);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<IEnumerable<VehicleDTO>> GetAllAsync()
        {
            var vehicles = await GetExtensions.GetAllVehicles(context).Select(v => new VehicleDTO(v)).ToListAsync();
            return vehicles;
        }

    }
}