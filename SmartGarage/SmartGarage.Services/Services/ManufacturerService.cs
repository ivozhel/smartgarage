﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Services.Helpers;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class ManufacturerService : IManufacturerService
    {
        private readonly SmartGarageContext context;
        private readonly IModelMapper modelMapper;

        public ManufacturerService(SmartGarageContext context, IModelMapper modelMapper)
        {
            this.context = context;
            this.modelMapper = modelMapper;
        }

        public async Task<ManufacturerDTO> GetAync(int id)
        {
            var manufacturer = await context.Manufacturers.FirstOrDefaultAsync(m => m.Id == id);
            if (manufacturer == null)
            {
                return null;
            }
            return new ManufacturerDTO(manufacturer);
        }
        public async Task<ManufacturerDTO> GetAync(string name)
        {
            var manufacturer = await context.Manufacturers.FirstOrDefaultAsync(m => m.Name == name);
            if (manufacturer == null)
            {
                return null;
            }
            return new ManufacturerDTO(manufacturer);
        }

        public async Task<Pager<ManufacturerDTO>> GetAllAync(PagerQueryObject pagerQuery)
        {
            var skip = (pagerQuery.PageNumber - 1) * pagerQuery.ItemsOnPage;
            var manufacturers = context.Manufacturers.OrderByDescending(s => s.Id);

            var manufacturersDTOs = await manufacturers.Where(x => !x.IsDeleted).Skip(skip)
                .Take(pagerQuery.ItemsOnPage)
                .Select(m => new ManufacturerDTO(m))
                .ToListAsync();

            Pager<ManufacturerDTO> result = new Pager<ManufacturerDTO>(manufacturersDTOs, pagerQuery, manufacturers.Count());

            return result;
        }

        public async Task<Pager<ManufacturerDTO>> GetAllDeletedAync(PagerQueryObject pagerQuery)
        {
            var skip = (pagerQuery.PageNumber - 1) * pagerQuery.ItemsOnPage;
            var manufacturers = context.Manufacturers;

            var manufacturersDTOs = await manufacturers.Where(x => x.IsDeleted).Skip(skip)
                .Take(pagerQuery.ItemsOnPage)
                .Select(m => new ManufacturerDTO(m))
                .ToListAsync();

            Pager<ManufacturerDTO> result = new Pager<ManufacturerDTO>(manufacturersDTOs, pagerQuery, manufacturers.Count());

            return result;
        }

        public async Task<IEnumerable<ManufacturerDTO>> GetAllAync()
        {
            //if there are no manufacturers make check
            return await context.Manufacturers.Select(m => new ManufacturerDTO(m)).ToListAsync();
        }

        public async Task<bool> CreateAync(ManufacturerDTO model)
        {
            var manufacturerToCreate = modelMapper.ToManufacturer(model);

            if (manufacturerToCreate == null)
            {
                return false;
            }

            manufacturerToCreate.Name = manufacturerToCreate.Name.First().ToString().ToUpper() + manufacturerToCreate.Name.Substring(1);
            await context.Manufacturers.AddAsync(manufacturerToCreate);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateAsync(ManufacturerDTO model, int id)
        {
            var manufacturer = await context.Manufacturers.FirstOrDefaultAsync(m => m.Id == id);

            if (manufacturer == null || model == null)
            {
                return false;
            }

            manufacturer.Name = model.Name ?? manufacturer.Name;

            context.Manufacturers.Update(manufacturer);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var manufacturer = await context.Manufacturers.FirstOrDefaultAsync(v => v.Id == id);
            if (manufacturer == null)
            {
                return false;
            }
            context.Manufacturers.Remove(manufacturer);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> RestoreAsync(int id)
        {
            var manufacturer = await context.Manufacturers.FirstOrDefaultAsync(v => v.Id == id);
            if (manufacturer == null)
            {
                return false;
            }
            manufacturer.IsDeleted = false;
            await context.SaveChangesAsync();

            return true;
        }
    }
}