﻿using SmartGarage.Services.Helpers;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services.Interfaces
{
    public interface IManufacturerService
    {
        public Task<Pager<ManufacturerDTO>> GetAllAync(PagerQueryObject pagerQuery);
        public Task<Pager<ManufacturerDTO>> GetAllDeletedAync(PagerQueryObject pagerQuery);
        public Task<ManufacturerDTO> GetAync(string name);
        public Task<IEnumerable<ManufacturerDTO>> GetAllAync();

        public Task<ManufacturerDTO> GetAync(int id);

        public Task<bool> CreateAync(ManufacturerDTO model);

        public Task<bool> UpdateAsync(ManufacturerDTO model, int id);

        public Task<bool> DeleteAsync(int id);
        public Task<bool> RestoreAsync(int id);
    }
}