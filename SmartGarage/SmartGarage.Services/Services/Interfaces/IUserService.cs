﻿using SmartGarage.Services.Helpers;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services.Interfaces
{
    public interface IUserService
    {
        //public Task<UserDTO> GetAsync(int id);
        public Task<Pager<UserDTO>> GetAllCustomersAsync(FilterUserQuery userQuery, PagerQueryObject pagerQuery);
        public Task DeleteForRealAsync(int id);
        public Task<UserDTO> GetCustomerAsync(int id);
        public Task<UserDTO> GetUserAsync(int id);
        public Task<bool> ResetPassword(string password, int id);
        public Task<bool> CreateCustomerAsync(CreateUserDTO model);
        public Task<IEnumerable<UserDTO>> GetAllCustomersAsync();

        public Task<bool> UpdateAsync(UpdateUserDTO model, int id);
        public Task<bool> DeleteAsync(int id);
    }
}