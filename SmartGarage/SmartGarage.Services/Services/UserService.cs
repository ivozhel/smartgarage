﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Extensions;
using SmartGarage.Services.Helpers;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class UserService : IUserService
    {
        private readonly SmartGarageContext context;
        private readonly IModelMapper modelMapper;

        public UserService(SmartGarageContext context, IModelMapper modelMapper)
        {
            this.context = context;
            this.modelMapper = modelMapper;
        }

        //public async Task<UserDTO> GetAsync(int id)
        //{
        //    var user = await context.Users.FirstOrDefaultAsync(u => u.Id == id);
        //    if (user == null)
        //    {
        //        return null;
        //    }
        //    return new UserDTO(user);
        //}

        public async Task<UserDTO> GetCustomerAsync(int id)
        {
            var customerIds = context.UserRoles.Where(r => r.RoleId == 2).Select(x => x.UserId);
            if (customerIds.Contains(id))
            {
                var customer = await context.Users
                    .Include(u => u.Vehicles).ThenInclude(x => x.Model.Manufacturer)
                    .FirstOrDefaultAsync(u => u.Id == id);
                return new UserDTO(customer);
            }

            return null;
        }
        public async Task<UserDTO> GetUserAsync(int id)
        {
                var customer = await context.Users
                    .Include(u => u.Vehicles).ThenInclude(x => x.Model.Manufacturer)
                    .FirstOrDefaultAsync(u => u.Id == id);
            if (customer != null)
            {
                return new UserDTO(customer);
            }
               

            return null;
        }
        public async Task<IEnumerable<UserDTO>> GetAllCustomersAsync()
        {
            var customerIds = context.UserRoles.Where(r => r.RoleId == 2).Select(x => x.UserId);
            //query split !
            return await context.Users.Include(c => c.Vehicles)
                .ThenInclude(v => v.Visits)
                .Include(c => c.Vehicles)
                .ThenInclude(v => v.Model.Manufacturer).Where(x => customerIds.Contains(x.Id))
                .Select(x => new UserDTO(x)).ToListAsync();
        }
        public async Task<Pager<UserDTO>> GetAllCustomersAsync(FilterUserQuery userQuery, PagerQueryObject pagerQuery)
        {
            //optimise
            var skip = (pagerQuery.PageNumber - 1) * pagerQuery.ItemsOnPage;
            var customerIds = context.UserRoles.Where(r => r.RoleId == 2).Select(x => x.UserId);
            //query split !
            var customers = context.Users.OrderByDescending(s => s.Id).Include(c => c.Vehicles)
                .ThenInclude(v => v.Visits)
                .Include(c => c.Vehicles)
                .ThenInclude(v => v.Model.Manufacturer).Where(x => customerIds.Contains(x.Id));

            if (userQuery.Name != null)
            {
                customers = customers.Where(c => (c.FirstName + " " + c.LastName).Contains(userQuery.Name));
            }

            if (userQuery.Email != null)
            {
                customers = customers.Where(c => (c.Email).Contains(userQuery.Email));
            }

            if (userQuery.Phone != null)
            {
                customers = customers.Where(c => (c.PhoneNumber).Contains(userQuery.Phone));
            }

            if (userQuery.Vehicle != null)
            {
                customers = customers.Where(c => (c.Vehicles.Any(v => (v.Model.Name + " " + v.Model.Manufacturer.Name).Contains(userQuery.Vehicle))));
            }

            if (userQuery.From != default || userQuery.To != default)
            {
                if (userQuery.DateFilter)
                {
                    if (userQuery.From != default && userQuery.To != default)
                    {
                        customers = customers.Where(c => (c.Vehicles.Any(v => v.Visits.Any(v => v.DateIn >= userQuery.From && v.DateIn <= userQuery.To))));
                    }
                    else if (userQuery.From != default)
                    {
                        customers = customers.Where(c => (c.Vehicles.Any(v => v.Visits.Any(v => v.DateIn >= userQuery.From))));
                    }
                    else
                    {
                        customers = customers.Where(c => (c.Vehicles.Any(v => v.Visits.Any(v => v.DateIn <= userQuery.To))));
                    }
                }
                else
                {
                    if (userQuery.From != default && userQuery.To != default)
                    {
                        customers = customers.Where(c => (c.Vehicles.Any(v => v.Visits.Any(v => v.DateOut >= userQuery.From && v.DateOut <= userQuery.To))));
                    }
                    else if (userQuery.From != default)
                    {
                        customers = customers.Where(c => (c.Vehicles.Any(v => v.Visits.Any(v => v.DateOut >= userQuery.From))));
                    }
                    else
                    {
                        customers = customers.Where(c => (c.Vehicles.Any(v => v.Visits.Any(v => v.DateOut <= userQuery.To))));
                    }

                }

            }
            switch (userQuery.SortBy)
            {
                case "name":
                    switch (userQuery.Order)
                    {
                        case "desc":
                            customers = customers.OrderByDescending(c => c.FirstName);
                            break;
                        default:
                            customers = customers.OrderBy(c => c.FirstName);
                            break;
                    }
                    break;
                case "date":
                        switch (userQuery.Order)
                        {
                            case "desc":
                               customers = customers.OrderByDescending(x=>x.LastVisit);
                                break;
                            default:
                            customers = customers.OrderBy(x => x.LastVisit);
                            break;
                        }

                    break;
                default:
                    break;
            }


            var customerDTOs = await customers.Skip(skip)
                .Take(pagerQuery.ItemsOnPage)
                .Select(x => new UserDTO(x))
                .ToListAsync();


            Pager<UserDTO> result = new Pager<UserDTO>(customerDTOs, pagerQuery, customers.Count());

            return result;
        }

        public async Task<bool> CreateCustomerAsync(CreateUserDTO model)
        {
            var customer = modelMapper.ToCustomer(model);
            var hesher = new PasswordHasher<User>();
            var customerPassword = CustomPasswordGenerator.CreateRandomPassword();
            customer.PasswordHash = hesher.HashPassword(customer, customerPassword);

            await context.Users.AddAsync(customer);
            var isSaved = await context.SaveChangesAsync();

            if (isSaved != 0)
            {
                await context.UserRoles.AddAsync(new IdentityUserRole<int> { RoleId = 2, UserId = customer.Id });
                await context.SaveChangesAsync();
                SendEmail.SendPassword(customerPassword, customer.Email);
                return true;
            }

            return false; //TODO: do we want to throw ActionFailed here?
        }

        public async Task<bool> UpdateAsync(UpdateUserDTO model, int id)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == id);

            if (user == null || model == null)
            {
                return false;
            }

            if (model.FirstName != default)
            {
                user.FirstName = model.FirstName;
            }

            if (model.LastName != default)
            {
                user.LastName = model.LastName;
            }

            if (model.Email != default)
            {
                user.Email = model.Email;
            }

            context.Users.Update(user);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task DeleteForRealAsync(int id)
        {
            var customer = await context.Users.Include(u => u.Vehicles).FirstOrDefaultAsync(v => v.Id == id);

            foreach (var item in customer.Vehicles)
            {
                context.Vehicles.Remove(item);
            }
            context.Users.Remove(customer);
            await context.SaveChangesAsync(true);
        }
        public async Task<bool> DeleteAsync(int id)
        {
            var customer = await context.Users.Include(u => u.Vehicles).FirstOrDefaultAsync(v => v.Id == id);
            if (customer == null)
            {
                return false;
            }
            foreach (var item in customer.Vehicles)
            {
                context.Vehicles.Remove(item);
            }
            context.Users.Remove(customer);
            await context.SaveChangesAsync();

            return true;
        }
        public async Task<bool> ResetPassword(string password,int id)
        {
            if (password.Length < 8)
            {
                return false;
            }

            var user = context.Users.FirstOrDefault(x => x.Id == id);
            var passHasher = new PasswordHasher<User>();

            user.PasswordHash = passHasher.HashPassword(user,password);

            context.Users.Update(user);
            await context.SaveChangesAsync();

            return true;
        }
    }
}