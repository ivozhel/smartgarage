﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Services.Models.CreateDTOs
{
    public class CreateVehicleDTO
    {

        [Required]
        [Display(Name = "Manufacturer")]
        public int ManufacturerId { get; set; }

        [Required]
        [Display(Name = "Model")]
        public int ModelId { get; set; }

        [Required]
        [Display(Name = "Customer")]
        public int CustomerId { get; set; }

        [Required]
        [Display(Name = "Plate Number")]
        [RegularExpression(@"[A-Z]{1-2}\d{4}[A-Z]{2}", ErrorMessage = "Invalid plate number")]
        public string PlateNumber { get; set; }

        [Required]
        [Range(0, 15, ErrorMessage = "Types are between 0 and 15")]
        public int Type { get; set; }

        [Required]
        [Display(Name = "Identity Number")]
        public string IdentityNumber { get; set; }
    }
}
