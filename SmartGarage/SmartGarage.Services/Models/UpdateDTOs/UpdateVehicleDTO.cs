﻿using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Services.Models.UpdateDTOs
{
    public class UpdateVehicleDTO
    {
        [Display(Name = "Manufacturer")]
        public int ManufacturerId { get; set; }

        [Display(Name = "Model")]
        public int ModelId { get; set; }

        [Display(Name = "Customer")]
        public int CustomerId { get; set; }

        [Display(Name = "Plate Number")]
        [RegularExpression(@"[A-Z]{1-2}\d{4}[A-Z]{2}", ErrorMessage = "Invalid plate number")]
        public string PlateNumber { get; set; }
        public int Type { get; set; }

        [Display(Name = "Identity Number")]
        public string IdentityNumber { get; set; }
    }
}
