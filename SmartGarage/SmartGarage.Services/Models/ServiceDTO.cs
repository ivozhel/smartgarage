﻿using SmartGarage.Data.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace SmartGarage.Services.Models
{
    public class ServiceDTO
    {
        public ServiceDTO()
        {

        }
        public ServiceDTO(Service model, decimal exchange, DateTime date)
        {
            this.Id = model.Id;
            this.Name = model.Name;
            this.Price = Math.Round(model.Price * exchange, 2);
            this.Description = model.Description;
        }

        public ServiceDTO(Service model)
        {
            this.Id = model.Id;
            this.Name = model.Name;
            this.Price = model.Price;
            this.Description = model.Description;
        }

        [JsonIgnore]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Price")]
        public decimal Price { get; set; }

        public string Description { get; set; }
        public bool AddToVisit { get; set; }
    }
}