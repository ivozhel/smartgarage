﻿using Newtonsoft.Json;
using SmartGarage.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace SmartGarage.Services.Models
{
    public class UserDTO
    {
        public UserDTO()
        {
        }

        public UserDTO(User model)
        {
            this.Id = model.Id;
            this.Email = model.Email;
            this.FirstName = model.FirstName;
            this.LastName = model.LastName;
            this.Name = model.FirstName + " " + model.LastName;
            this.Phone = model.PhoneNumber;
            this.Vehicles = model.Vehicles.Select(x => new VehicleDTO(x));

            if (model.LastVisit != null)
            {
                this.LastVisit = model.LastVisit.Value.ToString("dd/MM/yyyy");
            }
        }

        [JsonIgnore]
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public IEnumerable<VehicleDTO> Vehicles { get; set; }
        public string LastVisit { get; set; }
    }
}