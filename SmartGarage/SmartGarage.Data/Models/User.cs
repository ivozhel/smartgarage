﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace SmartGarage.Data.Models
{
    public class User : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<Vehicle> Vehicles { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastVisit { get; set; }
    }
}