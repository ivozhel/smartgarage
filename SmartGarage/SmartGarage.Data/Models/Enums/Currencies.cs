﻿namespace SmartGarage.Data.Models.Enums
{
    public enum Currencies
    {
        EUR,
        USD,
        BGN
    }
}
