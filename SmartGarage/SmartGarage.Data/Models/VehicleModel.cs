﻿using SmartGarage.Data.Models.Enums;

namespace SmartGarage.Data.Models
{
    public class VehicleModel
    {
        public int Id { get; set; }
        public int ManufacturerId { get; set; }
        public Manufacturer Manufacturer { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public VehicleTypes Type { get; set; }
    }
}