﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartGarage.Data.Models
{
    public class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [Column(TypeName = "decimal(19,2)")]
        public decimal Price { get; set; }

        public ICollection<Visit> Visits { get; set; }
        public bool IsDeleted { get; set; }

        public string Description { get; set; }
    }
}