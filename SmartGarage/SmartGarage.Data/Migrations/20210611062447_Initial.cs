﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartGarage.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastVisit = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Manufacturers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Manufacturers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(19,2)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleModels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ManufacturerId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleModels_Manufacturers_ManufacturerId",
                        column: x => x.ManufacturerId,
                        principalTable: "Manufacturers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ModelId = table.Column<int>(type: "int", nullable: false),
                    CustomerId = table.Column<int>(type: "int", nullable: false),
                    PlateNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    IdentityNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vehicles_AspNetUsers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Vehicles_VehicleModels_ModelId",
                        column: x => x.ModelId,
                        principalTable: "VehicleModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Visits",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VehicleId = table.Column<int>(type: "int", nullable: false),
                    DateIn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateOut = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Visits_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceVisit",
                columns: table => new
                {
                    ServicesId = table.Column<int>(type: "int", nullable: false),
                    VisitsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceVisit", x => new { x.ServicesId, x.VisitsId });
                    table.ForeignKey(
                        name: "FK_ServiceVisit_Services_ServicesId",
                        column: x => x.ServicesId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceVisit_Visits_VisitsId",
                        column: x => x.VisitsId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "eaf77297-659c-44d4-bce8-d9d07b7ef99b", "Employee", "EMPLOYEE" },
                    { 2, "65af2f04-adc3-4fa4-ab8b-123f9d2867ba", "Customer", "CUSTOMER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "IsDeleted", "LastName", "LastVisit", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 6, 0, "d7e335d7-7d67-4241-b46b-6149f8069f21", "annie@coruscant.com", false, "Anakin", false, "Skywalker", new DateTime(2021, 6, 8, 9, 24, 46, 24, DateTimeKind.Local).AddTicks(5915), false, null, "ANNIE@CORUSCANT.COM", "ANNIE@CORUSCANT.COM", null, "0666666666", false, "745cbf2a-c2f6-4bd9-b593-32a142dd2d5e", false, "annie@coruscant.com" },
                    { 5, 0, "3e1169f9-8343-4c83-b3a7-2184b757dcdd", "kubratismailov@gmail.com", false, "Kubrat", false, "Ismailov", new DateTime(2021, 4, 28, 9, 24, 46, 24, DateTimeKind.Local).AddTicks(5904), false, null, "KUBRATISMAILOV@GMAIL.COM", "KUBRATISMAILOV@GMAIL.COM", null, "0888888888", false, "fffa9abf-098a-4d7f-9c75-7aa97e8adf5e", false, "kubratismailov@gmail.com" },
                    { 4, 0, "bb599337-bcc1-4fbf-ae55-df818be28463", "irinahristova@gmail.com", false, "Irina", false, "Hristova", new DateTime(2021, 4, 28, 9, 24, 46, 24, DateTimeKind.Local).AddTicks(5888), false, null, "IRINAHRISTOVA@GMAIL.COM", "IRINAHRISTOVA@GMAIL.COM", null, "0888888888", false, "430e573b-9350-4356-a5e7-957c7e475f3f", false, "irinahristova@gmail.com" },
                    { 3, 0, "07b72a7c-1084-4f48-9871-7ef2b6ec7b67", "samuilpetkov@gmail.com", false, "Samuil", false, "Petkov", new DateTime(2021, 6, 10, 9, 24, 46, 24, DateTimeKind.Local).AddTicks(5817), false, null, "SAMUILPETKOV@GMAIL.COM", "SAMUILPETKOV@GMAIL.COM", null, "0888888888", false, "34be658c-9ccd-41b8-8b35-c9dcb57d3081", false, "samuilpetkov@gmail.com" },
                    { 2, 0, "69fc2572-0fde-4cc2-a53c-0a73776a9084", "georgiivanov@gmail.com", false, "Georgi", false, "Ivanov", new DateTime(2021, 6, 10, 9, 24, 46, 19, DateTimeKind.Local).AddTicks(8478), false, null, "GEORGIIVANOV@GMAIL.COM", "GEORGIIVANOV@GMAIL.COM", "AQAAAAEAACcQAAAAENnBeXEeqrYO3YS1xaXp1K2gNLLaikFGPNNJL8IKtxwKHbMmDoDb0w2aZ4rKnbRjiw==", "0888888888", false, "b375d9d4-a873-40f4-a901-3841ccbb4bcc", false, "georgiivanov@gmail.com" },
                    { 1, 0, "d23c60a9-a47b-474d-b8f4-560fb31acd4a", "ivanivanov@gmail.com", false, "Ivan", false, "Ivanov", null, false, null, "IVANIVANOV@GMAIL.COM", "IVANIVANOV@GMAIL.COM", "AQAAAAEAACcQAAAAEKckqiNhCgSv++tTi7plgxozf3w7sIIRrH9igkRvKlmSe+L9ocekFwx5ZogHRpss6w==", "0888888888", false, "0c3f191c-5940-44c0-8006-7b4d0dfa4fc9", false, "ivanivanov@gmail.com" },
                    { 7, 0, "c6ddfb48-bbde-4a4f-80c9-f20e79819a28", "yanavladimirova@gmail.com", false, "Yana", false, "Vladimirova", new DateTime(2021, 6, 1, 9, 24, 46, 24, DateTimeKind.Local).AddTicks(5932), false, null, "YANAVLADIMIROVA@GMAIL.COM", "YANAVLADIMIROVA@GMAIL.COM", null, "0888888888", false, "62f8c038-1fee-4887-aa8b-ec30efe2e917", false, "yanavladimirova@gmail.com" },
                    { 8, 0, "322a681d-0b17-47cb-aa00-6f9c6ff5acc8", "tristanshields@gmail.com", false, "Tristan", false, "Shields", new DateTime(2021, 6, 8, 9, 24, 46, 24, DateTimeKind.Local).AddTicks(5943), false, null, "TRISTANSHIELDS@GMAIL.COM", "TRISTANSHIELDS@GMAIL.COM", null, "0888888844", false, "51eca190-142a-4631-9114-7047f3de4042", false, "tristanshields@gmail.com" }
                });

            migrationBuilder.InsertData(
                table: "Manufacturers",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 9, false, "Chevrolet" },
                    { 10, false, "BMW" },
                    { 11, false, "Subaru" },
                    { 12, false, "Mercedes-Benz" },
                    { 7, false, "Lexus" },
                    { 1, false, "Ford" },
                    { 2, false, "Opel" },
                    { 3, false, "Skoda" },
                    { 4, false, "Fiat" },
                    { 8, false, "Jeep" },
                    { 5, false, "Toyota" },
                    { 6, false, "Honda" }
                });

            migrationBuilder.InsertData(
                table: "Services",
                columns: new[] { "Id", "Description", "IsDeleted", "Name", "Price" },
                values: new object[,]
                {
                    { 6, "Bring your vehicle to us if you dread replacing your tires on your own. Tires not included with this service.", false, "Tire change (full set)", 29.99m },
                    { 7, "We can repair any part that can be repaired. Costs vary depending on the nature of the damage and the time required for repair", false, "Part repairs", 100m },
                    { 5, "We can quickly replace any broken part of your vehicle. Part cost not included", false, "Part Change", 25.75m },
                    { 4, "The inside and outside of you vehicle are carefully cleaned and disinfected, we use a specialized hoover to remove pet hair and persistent stains.", false, "Deep Clean", 105.99m },
                    { 3, "Our experts can evaluate the functionality of your vehicle and suggest how to take good care of it", false, "Inspection", 50.99m },
                    { 2, "We can make your car look brand new with our state-of-the-art polishing products", false, "Polish", 29.99m },
                    { 8, "You are always welcome to stay around and have a cup of coffee with us", false, "Coffee", 0m },
                    { 1, "A through and careful cleaning of the outside of your vehicle", false, "Clean", 39.99m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { 2, 8 },
                    { 2, 6 },
                    { 2, 5 },
                    { 2, 4 },
                    { 2, 3 },
                    { 2, 2 },
                    { 1, 1 },
                    { 2, 7 }
                });

            migrationBuilder.InsertData(
                table: "VehicleModels",
                columns: new[] { "Id", "IsDeleted", "ManufacturerId", "Name", "Type" },
                values: new object[,]
                {
                    { 12, false, 12, "GLA", 0 },
                    { 11, false, 11, "Outback", 0 },
                    { 10, false, 10, "Z4 ", 0 },
                    { 8, false, 8, "Wrangler", 0 },
                    { 7, false, 7, "CT", 0 },
                    { 6, false, 6, "Accord", 0 },
                    { 5, false, 5, "Avensis", 0 },
                    { 4, false, 4, "Punto", 0 },
                    { 3, false, 3, "Oktavia", 0 },
                    { 2, false, 2, "Astra", 0 },
                    { 9, false, 9, "Blazer", 0 },
                    { 1, false, 1, "Ka", 0 }
                });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "CustomerId", "IdentityNumber", "IsDeleted", "ModelId", "PlateNumber", "Type" },
                values: new object[,]
                {
                    { 1, 2, "W6654654846465464", false, 1, "PB1824CM", 1 },
                    { 15, 4, "W665NFHJK76465464", false, 10, "CA0294KO", 1 },
                    { 14, 5, "W6654654ARH865464", false, 9, "CA4526OK", 2 },
                    { 13, 8, "W66546OPSGLP65464", false, 8, "CB8203HM", 1 },
                    { 12, 7, "W665NOPS8DSA65464", false, 7, "CO4375AP", 3 },
                    { 11, 6, "W665465467DHA5464", false, 6, "PB7834PH", 1 },
                    { 10, 3, "W6654654ASDSA5464", false, 5, "CA5555BA", 2 },
                    { 16, 2, "W665NOP89IK465464", false, 11, "TH4375AP", 3 },
                    { 8, 7, "W665NOPS846465464", false, 4, "TH4375AK", 3 },
                    { 6, 5, "W6654654ASGH65464", false, 3, "A4526OP", 2 },
                    { 3, 3, "W6654654846SLPQ64", false, 3, "PB7000KM", 5 },
                    { 9, 8, "W66546OPSN6465464", false, 2, "B8203FS", 1 },
                    { 5, 5, "W66546DLOP6465464", false, 2, "CO1234KX", 1 },
                    { 2, 3, "W6654654846SMJRT4", false, 2, "CA7774K", 2 },
                    { 7, 6, "W665NFHS846465464", false, 1, "CA0294KO", 1 },
                    { 4, 4, "W6654654846SKOP64", false, 4, "PB7274CM", 4 },
                    { 17, 8, "W66546RJK09465464", false, 12, "CA8203TE", 1 }
                });

            migrationBuilder.InsertData(
                table: "Visits",
                columns: new[] { "Id", "DateIn", "DateOut", "IsDeleted", "Status", "VehicleId" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 6, 9, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(1559), new DateTime(2021, 6, 10, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(2848), false, 0, 1 },
                    { 15, new DateTime(2021, 4, 28, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6235), null, false, 1, 15 },
                    { 14, new DateTime(2021, 4, 20, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6228), null, false, 1, 14 },
                    { 13, new DateTime(2021, 2, 8, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6221), null, false, 1, 13 },
                    { 12, new DateTime(2021, 6, 1, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6215), null, false, 2, 12 },
                    { 11, new DateTime(2021, 6, 8, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6207), null, false, 2, 11 },
                    { 10, new DateTime(2021, 6, 10, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6200), null, false, 0, 10 },
                    { 8, new DateTime(2021, 5, 9, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6170), new DateTime(2021, 5, 20, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6176), false, 0, 8 },
                    { 4, new DateTime(2021, 2, 8, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6140), null, false, 1, 4 },
                    { 6, new DateTime(2021, 4, 28, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6157), null, false, 1, 6 },
                    { 3, new DateTime(2021, 6, 1, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6131), null, false, 2, 3 },
                    { 9, new DateTime(2021, 5, 25, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6185), new DateTime(2021, 5, 30, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6193), false, 0, 9 },
                    { 5, new DateTime(2021, 4, 20, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6150), null, false, 1, 5 },
                    { 18, new DateTime(2021, 6, 4, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6261), new DateTime(2021, 6, 9, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6267), false, 0, 2 },
                    { 2, new DateTime(2021, 6, 8, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6101), null, false, 2, 2 },
                    { 7, new DateTime(2021, 5, 20, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6164), null, false, 1, 7 },
                    { 16, new DateTime(2021, 5, 20, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6242), null, false, 1, 16 },
                    { 17, new DateTime(2021, 6, 8, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6249), new DateTime(2021, 6, 10, 9, 24, 46, 76, DateTimeKind.Local).AddTicks(6254), false, 0, 17 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Manufacturers_Name",
                table: "Manufacturers",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceVisit_VisitsId",
                table: "ServiceVisit",
                column: "VisitsId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleModels_ManufacturerId",
                table: "VehicleModels",
                column: "ManufacturerId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_CustomerId",
                table: "Vehicles",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_ModelId",
                table: "Vehicles",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Visits_VehicleId",
                table: "Visits",
                column: "VehicleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ServiceVisit");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Services");

            migrationBuilder.DropTable(
                name: "Visits");

            migrationBuilder.DropTable(
                name: "Vehicles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "VehicleModels");

            migrationBuilder.DropTable(
                name: "Manufacturers");
        }
    }
}
