﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;

namespace SmartGarage.Data.Configurations
{
    internal class ModelConfig : IEntityTypeConfiguration<VehicleModel>
    {
        public void Configure(EntityTypeBuilder<VehicleModel> builder)
        {
            var models = new VehicleModel[]
            {
                new VehicleModel {Id = 1, ManufacturerId = 1, Name = "Ka"},
                new VehicleModel {Id = 2, ManufacturerId = 2, Name = "Astra"},
                new VehicleModel {Id = 3, ManufacturerId = 3, Name = "Oktavia"},
                new VehicleModel {Id = 4, ManufacturerId = 4, Name = "Punto"},
                new VehicleModel {Id = 5, ManufacturerId = 5, Name = "Avensis"},
                new VehicleModel {Id = 6, ManufacturerId = 6, Name = "Accord"},
                new VehicleModel {Id = 7, ManufacturerId = 7, Name = "CT"},
                new VehicleModel {Id = 8, ManufacturerId = 8, Name = "Wrangler"},
                new VehicleModel {Id = 9, ManufacturerId = 9, Name = "Blazer"},
                new VehicleModel {Id = 10, ManufacturerId = 10, Name = "Z4 "},
                new VehicleModel {Id = 11, ManufacturerId = 11, Name = "Outback"},
                new VehicleModel {Id = 12, ManufacturerId = 12, Name = "GLA"}
            };
            builder.Property<bool>("IsDeleted");
            builder.HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);

            builder.HasData(models);
        }
    }
}