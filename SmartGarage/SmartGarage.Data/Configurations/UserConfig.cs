﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;
using System;

namespace SmartGarage.Data.Configurations
{
    internal class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            var passHasher = new PasswordHasher<User>();
            var users = new User[]
            {
                new User
                {
                    Id = 1,
                    Email = "ivanivanov@gmail.com",
                    NormalizedEmail = "IVANIVANOV@GMAIL.COM",
                    UserName = "ivanivanov@gmail.com",
                    NormalizedUserName = "IVANIVANOV@GMAIL.COM",
                    FirstName = "Ivan",
                    LastName = "Ivanov",
                    PhoneNumber = "0888888888",
                    SecurityStamp = Guid.NewGuid().ToString()
                },
                new User
                {
                    Id = 2,
                    Email = "georgiivanov@gmail.com",
                    UserName ="georgiivanov@gmail.com",
                    NormalizedUserName = "GEORGIIVANOV@GMAIL.COM",
                    NormalizedEmail = "GEORGIIVANOV@GMAIL.COM",
                    FirstName = "Georgi",
                    LastName = "Ivanov",
                    PhoneNumber = "0888888888",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LastVisit =  DateTime.Now.AddDays(-1)
                },
                new User
                {
                    Id = 3,
                    Email = "samuilpetkov@gmail.com",
                    UserName ="samuilpetkov@gmail.com",
                    NormalizedUserName = "SAMUILPETKOV@GMAIL.COM",
                    NormalizedEmail = "SAMUILPETKOV@GMAIL.COM",
                    FirstName = "Samuil",
                    LastName = "Petkov",
                    PhoneNumber = "0888888888",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LastVisit = DateTime.Now.AddDays(-1)
                },
                new User
                {
                    Id = 4,
                    Email = "irinahristova@gmail.com",
                    UserName ="irinahristova@gmail.com",
                    NormalizedUserName = "IRINAHRISTOVA@GMAIL.COM",
                    NormalizedEmail = "IRINAHRISTOVA@GMAIL.COM",
                    FirstName = "Irina",
                    LastName = "Hristova",
                    PhoneNumber = "0888888888",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LastVisit = DateTime.Now.AddDays(-44)

                },
                new User
                {
                    Id = 5,
                    Email = "kubratismailov@gmail.com",
                    UserName ="kubratismailov@gmail.com",
                    NormalizedUserName = "KUBRATISMAILOV@GMAIL.COM",
                    NormalizedEmail = "KUBRATISMAILOV@GMAIL.COM",
                    FirstName = "Kubrat",
                    LastName = "Ismailov",
                    PhoneNumber = "0888888888",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LastVisit = DateTime.Now.AddDays(-44)
                },
                new User
                {
                    Id = 6,
                    Email = "annie@coruscant.com",
                    UserName ="annie@coruscant.com",
                    NormalizedUserName = "ANNIE@CORUSCANT.COM",
                    NormalizedEmail = "ANNIE@CORUSCANT.COM",
                    FirstName = "Anakin",
                    LastName = "Skywalker",
                    PhoneNumber = "0666666666",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LastVisit = DateTime.Now.AddDays(-3),
                },
                new User
                {
                    Id = 7,
                    Email = "yanavladimirova@gmail.com",
                    UserName ="yanavladimirova@gmail.com",
                    NormalizedUserName = "YANAVLADIMIROVA@GMAIL.COM",
                    NormalizedEmail = "YANAVLADIMIROVA@GMAIL.COM",
                    FirstName = "Yana",
                    LastName = "Vladimirova",
                    PhoneNumber = "0888888888",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LastVisit = DateTime.Now.AddDays(-10)
                },
                new User
                {
                    Id = 8,
                    Email = "tristanshields@gmail.com",
                    UserName ="tristanshields@gmail.com",
                    NormalizedUserName = "TRISTANSHIELDS@GMAIL.COM",
                    NormalizedEmail = "TRISTANSHIELDS@GMAIL.COM",
                    FirstName = "Tristan",
                    LastName = "Shields",
                    PhoneNumber = "0888888844",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    LastVisit = DateTime.Now.AddDays(-3)
                },
            };

            users[0].PasswordHash = passHasher.HashPassword(users[0], "Vankata123");
            users[1].PasswordHash = passHasher.HashPassword(users[1], "Goshkata123");

            builder.Property<bool>("IsDeleted");
            builder.HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);
            builder.HasData(users);

        }
    }
}