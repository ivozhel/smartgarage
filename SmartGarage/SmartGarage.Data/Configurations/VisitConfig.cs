﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;
using System;

namespace SmartGarage.Data.Configurations
{
    internal class VisitConfig : IEntityTypeConfiguration<Visit>
    {
        public void Configure(EntityTypeBuilder<Visit> builder)
        {
            var visits = new Visit[]
            {
                new Visit{Id = 1, DateIn = DateTime.Now.AddDays(-2), DateOut = DateTime.Now.AddDays(-1), Status = 0, VehicleId = 1 },
                new Visit{Id = 2, DateIn = DateTime.Now.AddDays(-3), Status = (Models.Enums.Status)2, VehicleId = 2 },
                new Visit{Id = 3, DateIn = DateTime.Now.AddDays(-10), Status = (Models.Enums.Status)2, VehicleId = 3 },
                new Visit{Id = 4, DateIn = DateTime.Now.AddDays(-123), Status = (Models.Enums.Status)1, VehicleId = 4 },
                new Visit{Id = 5, DateIn = DateTime.Now.AddDays(-52), Status = (Models.Enums.Status)1, VehicleId = 5 },
                new Visit{Id = 6, DateIn = DateTime.Now.AddDays(-44), Status = (Models.Enums.Status)1, VehicleId = 6 },
                new Visit{Id = 7, DateIn = DateTime.Now.AddDays(-22), Status = (Models.Enums.Status)1, VehicleId = 7 },
                new Visit{Id = 8, DateIn = DateTime.Now.AddDays(-33), DateOut = DateTime.Now.AddDays(-22), Status = (Models.Enums.Status)0, VehicleId = 8 },
                new Visit{Id = 9, DateIn = DateTime.Now.AddDays(-17), DateOut = DateTime.Now.AddDays(-12), Status = (Models.Enums.Status)0, VehicleId = 9 },
                new Visit{Id = 10, DateIn = DateTime.Now.AddDays(-1), Status = 0, VehicleId = 10 },
                new Visit{Id = 11, DateIn = DateTime.Now.AddDays(-3), Status = (Models.Enums.Status)2, VehicleId = 11 },
                new Visit{Id = 12, DateIn = DateTime.Now.AddDays(-10), Status = (Models.Enums.Status)2, VehicleId = 12 },
                new Visit{Id = 13, DateIn = DateTime.Now.AddDays(-123), Status = (Models.Enums.Status)1, VehicleId = 13 },
                new Visit{Id = 14, DateIn = DateTime.Now.AddDays(-52), Status = (Models.Enums.Status)1, VehicleId = 14 },
                new Visit{Id = 15, DateIn = DateTime.Now.AddDays(-44), Status = (Models.Enums.Status)1, VehicleId = 15 },
                new Visit{Id = 16, DateIn = DateTime.Now.AddDays(-22), Status = (Models.Enums.Status)1, VehicleId = 16 },
                new Visit{Id = 17, DateIn = DateTime.Now.AddDays(-3), DateOut = DateTime.Now.AddDays(-1), Status = (Models.Enums.Status)0, VehicleId = 17 },
                new Visit{Id = 18, DateIn = DateTime.Now.AddDays(-7), DateOut = DateTime.Now.AddDays(-2), Status = (Models.Enums.Status)0, VehicleId = 2 },
            };
            builder.HasData(visits);
        }
    }
}