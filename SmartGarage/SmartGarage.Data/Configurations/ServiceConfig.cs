﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;

namespace SmartGarage.Data.Configurations
{
    internal class ServiceConfig : IEntityTypeConfiguration<Service>
    {
        public void Configure(EntityTypeBuilder<Service> builder)
        {
            var services = new Service[]
            {
                new Service { Id = 1, Name = "Clean", Price = 39.99m, Description = "A through and careful cleaning of the outside of your vehicle" },
                new Service { Id = 2, Name = "Polish", Price = 29.99m,  Description = "We can make your car look brand new with our state-of-the-art polishing products" },
                new Service { Id = 3, Name = "Inspection", Price = 50.99m,  Description = "Our experts can evaluate the functionality of your vehicle and suggest how to take good care of it" },
                new Service { Id = 4, Name = "Deep Clean", Price = 105.99m,  Description = "The inside and outside of you vehicle are carefully cleaned and disinfected, we use a specialized hoover to remove pet hair and persistent stains." },
                new Service { Id = 5, Name = "Part Change", Price = 25.75m,  Description = "We can quickly replace any broken part of your vehicle. Part cost not included" },
                new Service { Id = 6, Name = "Tire change (full set)", Price = 29.99m,  Description = "Bring your vehicle to us if you dread replacing your tires on your own. Tires not included with this service." },
                new Service { Id = 7, Name = "Part repairs", Price = 100m, Description = "We can repair any part that can be repaired. Costs vary depending on the nature of the damage and the time required for repair" },
                new Service { Id = 8, Name = "Coffee", Price = 0m,  Description = "You are always welcome to stay around and have a cup of coffee with us" }
            };
            builder.Property<bool>("IsDeleted");
            builder.HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);

            builder.HasData(services);
        }
    }
}