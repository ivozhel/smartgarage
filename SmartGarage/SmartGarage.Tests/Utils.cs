﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using System;


namespace SmartGarage.Tests
{
    public class Utils
    {
        public static DbContextOptions<SmartGarageContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<SmartGarageContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
        public static IdentityRole<int>[] IdentityRoles()
        {
            var roles = new IdentityRole<int>[]
            {
                new IdentityRole<int> { Id = 1, Name = "Employee", NormalizedName = "EMPLOYEE" },
                new IdentityRole<int> { Id = 2, Name = "Customer", NormalizedName = "CUSTOMER" }
            };
            return roles;

        }
        public static User[] GetUsers()
        {
            var passHasher = new PasswordHasher<User>();
            var users = new User[]
            {
                new User
                {
                    Id = 1,
                    Email = "ivanivanov@gmail.com",
                    NormalizedEmail = "IVANIVANOV@GMAIL.COM",
                    UserName = "ivanivanov@gmail.com",
                    NormalizedUserName = "IVANIVANOV@GMAIL.COM",
                    FirstName = "Ivan",
                    LastName = "Ivanov",
                    PhoneNumber = "0888888887",
                    SecurityStamp = Guid.NewGuid().ToString()

                },
                new User
                {
                    Id = 2,
                    Email = "georgiivanov@gmail.com",
                    UserName ="georgiivanov@gmail.com",
                    NormalizedUserName = "GEORGIIVANOV@GMAIL.COM",
                    NormalizedEmail = "GEORGIIVANOV@GMAIL.COM",
                    FirstName = "Georgi",
                    LastName = "Ivanov",
                    PhoneNumber = "0888888888",
                    SecurityStamp = Guid.NewGuid().ToString()
                },
                new User
                {
                    Id = 3,
                    Email = "samuilpetkov@gmail.com",
                    UserName ="samuilpetkov@gmail.com",
                    NormalizedUserName = "SAMUILPETKOV@GMAIL.COM",
                    NormalizedEmail = "SAMUILPETKOV@GMAIL.COM",
                    FirstName = "Samuil",
                    LastName = "Petkov",
                    PhoneNumber = "0888888889",
                    SecurityStamp = Guid.NewGuid().ToString()
                },
            };
            users[0].PasswordHash = passHasher.HashPassword(users[0], "Vankata123");
            users[1].PasswordHash = passHasher.HashPassword(users[1], "Goshkata123");
            return users;
        }
        public static IdentityUserRole<int>[] GetUserRoles()
        {
            var roles = new IdentityUserRole<int>[]
            {
                new IdentityUserRole<int> { RoleId = 1, UserId = 1 },
                new IdentityUserRole<int> { RoleId = 2 , UserId = 2 },
                new IdentityUserRole<int> { RoleId = 2, UserId = 3 }
            };
            return roles;
        }
        public static Vehicle[] GetVehicles()
        {
            var vehicles = new Vehicle[]
            {
               new Vehicle{Id = 1,ModelId = 1, CustomerId = 2,PlateNumber="PB1824CM", IdentityNumber = "W6654654846465464",Type = (Data.Models.Enums.VehicleTypes)1},
               new Vehicle{Id = 2,ModelId = 2, CustomerId = 3,PlateNumber="CA7774KH", IdentityNumber = "W9889984DSD465464",Type = (Data.Models.Enums.VehicleTypes)2},
               new Vehicle{Id = 3,ModelId = 3, CustomerId = 3,PlateNumber="XR7000KM", IdentityNumber = "W988989984ADSd647",Type = (Data.Models.Enums.VehicleTypes)5},

            };
            return vehicles;
        }
        public static Service[] GetServices()
        {
            var services = new Service[]
            {
                new Service { Id = 1, Name = "Clean", Price = 39.99m },
                new Service { Id = 2, Name = "Polish", Price = 29.99m },
                new Service { Id = 3, Name = "Inspection", Price = 50.99m }
           };
            return services;
        }
        public static Visit[] GetVisits()
        {
            var visits = new Visit[]
            {
                new Visit{Id = 1, DateIn = DateTime.Now.AddDays(-1), DateOut = DateTime.Now.AddDays(1),Status = (Data.Models.Enums.Status)1, VehicleId = 1 },
                new Visit{Id = 2, DateIn = DateTime.Now.AddDays(-3), Status = (Data.Models.Enums.Status)0, VehicleId = 2 },
                new Visit{Id = 3, DateIn = DateTime.Now.AddDays(-10), Status = (Data.Models.Enums.Status)2, VehicleId = 3 }
            };
            return visits;
        }
        public static VehicleModel[] GetVehicleModels()
        {
            var models = new VehicleModel[]
             {
                new VehicleModel {Id = 1, ManufacturerId = 1, Name = "Ka"},
                new VehicleModel {Id = 2, ManufacturerId = 2, Name = "Astra"},
                new VehicleModel {Id = 3, ManufacturerId = 3, Name = "Oktavia"},
                new VehicleModel {Id = 4, ManufacturerId = 4, Name = "Punto"}
             };
            return models;

        }
        public static Manufacturer[] GetManufacturers()
        {
            var manufacturers = new Manufacturer[]
            {
                new Manufacturer {Id = 1, Name = "Ford"},
                new Manufacturer {Id = 2, Name = "Opel"},
                new Manufacturer {Id = 3, Name = "Skoda"},
                new Manufacturer {Id = 4, Name = "Fiat"}
            };
            return manufacturers;
        }

        public static void FillBase(SmartGarageContext context)
        {
            context.Manufacturers.AddRange(GetManufacturers());
            context.VehicleModels.AddRange(GetVehicleModels());
            context.Visits.AddRange(GetVisits());
            context.Services.AddRange(GetServices());
            context.Users.AddRange(GetUsers());
            context.UserRoles.AddRange(GetUserRoles());
            context.Roles.AddRange(IdentityRoles());
            context.Vehicles.AddRange(GetVehicles());

        }

    }
}
