﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.ManufacturerServiceTests
{
    [TestClass]
    public class ManufaturerGet_Should
    {
        [TestMethod]
        public async Task ManufaturerGetAllCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("ManufaturerGetAllCorrectly");
            var manufaturers = Utils.GetManufacturers();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAync();

                //Assert
                Assert.AreEqual(manufaturers.Count(), result.Count());

            }

        }
        [TestMethod]
        public async Task ManufaturerGetAllCorrectlyWithPager()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("ManufaturerGetAllCorrectlyWithPager");
            var manufaturers = Utils.GetManufacturers();
            PagerQueryObject pagerQuery = new PagerQueryObject(1, manufaturers.Count() - 1);

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAync(pagerQuery);

                //Assert
                Assert.AreEqual(manufaturers.Count() - 1, result.Items.Count);

            }

        }
        [TestMethod]
        public async Task ManufaturerGetByIdCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("ManufaturerGetByIdCorrectly");
            var manufaturer = Utils.GetManufacturers().FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(actContext, modelMapperFake.Object);
                var result = await sut.GetAync(manufaturer.Id);

                //Assert
                Assert.AreEqual(manufaturer.Id, result.Id);
                Assert.AreEqual(manufaturer.Name, result.Name);

            }

        }
        [TestMethod]
        public async Task ManufaturerGetByIdReturnNullForNotFound()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("ManufaturerGetByIdReturnNullForNotFound");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(actContext, modelMapperFake.Object);
                var result = await sut.GetAync(0);

                //Assert
                Assert.IsNull(result);

            }

        }
    }
}
