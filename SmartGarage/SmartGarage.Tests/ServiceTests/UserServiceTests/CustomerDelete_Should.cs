﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.UserServiceTests
{
    [TestClass]
    public class CustomerDelete_Should
    {
        [TestMethod]
        public async Task UserDeleteCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("UserDeleteCorrectly");
            var userModelToDelete = Utils.GetUsers().FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapper);
                var result = await sut.DeleteAsync(userModelToDelete.Id);

                //Assert
                Assert.IsTrue(result);
                Assert.IsTrue(actContext.Users.IgnoreQueryFilters().FirstOrDefault(x => x.Id == userModelToDelete.Id).IsDeleted);

            }

        }
        [TestMethod]
        public async Task UserDeleteWrongID()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("UserDeleteWrongID");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapper);
                var result = await sut.DeleteAsync(0);

                //Assert
                Assert.IsFalse(result);

            }
        }
    }
}

