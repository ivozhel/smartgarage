﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.UserServiceTests
{
    public class CustomerUpdate_Should
    {
        [TestClass]
        public class ServiceUpdate_Should
        {
            [TestMethod]
            public async Task UserUpdateCorrectly()
            {
                //TODO: can be improved by validating the exact values of the model and the returned new service price
                var modelMapper = new ModelMapper();
                var options = Utils.GetOptions("UserUpdateCorrectly");
                var user = Utils.GetUsers().FirstOrDefault();
                var model = new UpdateUserDTO();
                model.FirstName = "Place";

                using (var arrangeContext = new SmartGarageContext(options))
                {
                    Utils.FillBase(arrangeContext);
                    await arrangeContext.SaveChangesAsync();
                }
                //Act
                using (var actContext = new SmartGarageContext(options))
                {
                    var sut = new UserService(actContext, modelMapper);
                    var result = await sut.UpdateAsync(model, 1);

                    //Assert
                    Assert.IsTrue(result);
                }

            }

            [TestMethod]
            public async Task UserUpdateWrongID()
            {
                var modelMapper = new ModelMapper();
                var options = Utils.GetOptions("UserUpdateWrongID");
                var model = new UpdateUserDTO();
                model.FirstName = "Holder";


                using (var arrangeContext = new SmartGarageContext(options))
                {
                    Utils.FillBase(arrangeContext);
                    await arrangeContext.SaveChangesAsync();
                }
                //Act
                using (var actContext = new SmartGarageContext(options))
                {
                    var sut = new UserService(actContext, modelMapper);
                    var result = await sut.UpdateAsync(model, 0);

                    //Assert
                    Assert.IsFalse(result);
                }
            }
        }
    }
}

