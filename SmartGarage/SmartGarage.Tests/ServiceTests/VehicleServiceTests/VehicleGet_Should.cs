﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VehicleServiceTests
{
    [TestClass]
    public class VehicleGet_Should
    {
        [TestMethod]
        public async Task VehicleGetAllCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VehicleGetAllCorrectly");
            var vehicleModels = Utils.GetVehicles();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync();

                //Assert
                Assert.AreEqual(vehicleModels.Count(), result.Count());

            }

        }

        [TestMethod]
        public async Task VehiclesGetAllCorrectlyWithPager()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VehiclesGetAllCorrectlyWithPager");
            var vehicleModels = Utils.GetVehicles();
            PagerQueryObject pagerQuery = new PagerQueryObject(1, vehicleModels.Count() - 1);

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(null, pagerQuery);

                //Assert
                Assert.AreEqual(vehicleModels.Count() - 1, result.Items.Count);

            }

        }

        [TestMethod]
        public async Task VehicleGetByIdCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VehicleGetByIdCorrectly");
            var vehicleModel = Utils.GetVehicles().FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleService(actContext, modelMapperFake.Object);
                var result = await sut.GetAsync(vehicleModel.Id);

                //Assert
                Assert.AreEqual(vehicleModel.Id, result.Id);
                Assert.AreEqual(vehicleModel.IdentityNumber, result.IdentityNumber);

            }

        }

        [TestMethod]
        public async Task VehicleGetByIdReturnNullForNotFound()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VehicleGetByIdReturnNullForNotFound");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleService(actContext, modelMapperFake.Object);
                var result = await sut.GetAsync(0);

                //Assert
                Assert.IsNull(result);

            }

        }

        [TestMethod]
        public async Task VehiclesGetAllSearch()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VehiclesGetAllSearch");
            var vehicleModels = Utils.GetVehicles();
            var cutomerIds = Utils.GetUserRoles().Where(x => x.RoleId == 2).Select(x => x.UserId);

            var customerForSearch = Utils.GetUsers().FirstOrDefault(x => cutomerIds.Contains(x.Id));
            var vehiclesCount = Utils.GetVehicles().Where(x => x.CustomerId == customerForSearch.Id).Count();

            PagerQueryObject pagerQuery = new PagerQueryObject(1, vehiclesCount);

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(customerForSearch.FirstName, pagerQuery);

                //Assert
                Assert.AreEqual(vehiclesCount, result.Items.Count);

            }

        }


    }
}
