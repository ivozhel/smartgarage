﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VehicleServiceTests
{
    [TestClass]
    public class VehicleCreate_Should
    {
        [TestMethod]
        public async Task VehicleCreateCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VehicleCreateCorrectly");
            var vehiclesCount = Utils.GetVehicles().Count();
            var manufacturer = Utils.GetManufacturers().FirstOrDefault();
            var customerIds = Utils.GetUserRoles().Where(x => x.RoleId == 2).Select(x => x.UserId);
            var model = new CreateVehicleDTO()
            {
                ManufacturerId = manufacturer.Id,
                ModelId = Utils.GetVehicleModels().FirstOrDefault(x => x.ManufacturerId == manufacturer.Id).Id,
                CustomerId = Utils.GetUsers().FirstOrDefault(x => customerIds.Contains(x.Id)).Id,
                IdentityNumber = "W0654654846465469",
                PlateNumber = "K1234CB",
                Type = 1
            };


            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleService(actContext, modelMapper);
                var result = await sut.CreateAsync(model);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(vehiclesCount + 1, actContext.Vehicles.Count());

            }

        }
    }
}
