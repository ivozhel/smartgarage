﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VehicleModelTests
{
    [TestClass]
    public class VehicleModelGet_Should
    {
        [TestMethod]
        public async Task VehicleModelGetAllCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VehicleModelGetAllCorrectly");
            var vehicleModels = Utils.GetVehicleModels();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleModelService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync();

                //Assert
                Assert.AreEqual(vehicleModels.Count(), result.Count());

            }

        }

        //TODO: fix this test
        //[TestMethod]
        //public async Task VehicleModelsGetAllCorrectlyWithPager()
        //{
        //    var modelMapperFake = new Mock<IModelMapper>();
        //    var options = Utils.GetOptions("VehicleModelsGetAllCorrectlyWithPager");
        //    var vehicleModels = Utils.GetVehicleModels();
        //    PagerQueryObject pagerQuery = new PagerQueryObject(1, vehicleModels.Count() - 1);

        //    using (var arrangeContext = new SmartGarageContext(options))
        //    {
        //        Utils.FillBase(arrangeContext);
        //        await arrangeContext.SaveChangesAsync();
        //    }
        //    //Act
        //    using (var actContext = new SmartGarageContext(options))
        //    {
        //        var sut = new VehicleModelService(actContext, modelMapperFake.Object);
        //        var result = await sut.GetAllAsync(pagerQuery);

        //        //Assert
        //        Assert.AreEqual(vehicleModels.Count() - 1, result.Items.Count);

        //    }

        //}
        [TestMethod]
        public async Task VehicleModelGetByIdCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VehicleModelGetByIdCorrectly");
            var vehicleModel = Utils.GetVehicleModels().FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleModelService(actContext, modelMapperFake.Object);
                var result = await sut.GetAsync(vehicleModel.Id);

                //Assert
                Assert.AreEqual(vehicleModel.Id, result.Id);
                Assert.AreEqual(vehicleModel.Name, result.Name);

            }

        }
        [TestMethod]
        public async Task VehicleModelGetByIdReturnNullForNotFound()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VehicleModelGetByIdReturnNullForNotFound");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleModelService(actContext, modelMapperFake.Object);
                var result = await sut.GetAsync(0);

                //Assert
                Assert.IsNull(result);

            }

        }
    }
}
