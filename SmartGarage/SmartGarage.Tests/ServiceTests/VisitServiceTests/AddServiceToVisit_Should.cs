﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VisitServiceTests
{
    [TestClass]
    public class AddServiceToVisit_Should
    {
        [TestMethod]
        public async Task AddServiceToVisitCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("AddServiceToVisitCorrectly");
            var visit = Utils.GetVisits().FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapper);
                var result = await sut.AddServiceToVisit(visit.Id, 1);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task AddServiceToVisitInvalidServiceId()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("AddServiceToVisitInvalidServiceId");
            var visit = Utils.GetVisits().FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapper);
                var result = await sut.AddServiceToVisit(visit.Id, 23);

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public async Task AddServiceToVisitInvalidVisitId()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("AddServiceToVisitInvalidVisitId");
            var service = Utils.GetServices().FirstOrDefault();
            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapper);
                var result = await sut.AddServiceToVisit(123, service.Id);
            }
        }
    }
}
