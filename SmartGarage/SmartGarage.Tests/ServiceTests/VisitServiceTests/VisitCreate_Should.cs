﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VisitServiceTests
{
    //TODO: test exceptions thrown on create
    [TestClass]
    public class VisitCreate_Should
    {
        [TestMethod]
        public async Task VisitModelCreateCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VisitModelCreateCorrectly");
            var visitModelsCount = Utils.GetVisits().Count();
            var model = new CreateVisitDTO();
            //model.CustomerId = 1;
            model.VehicleId = 1;

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapper);
                var result = await sut.CreateAsync(model);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(visitModelsCount + 1, actContext.Visits.Count());
            }
        }

    }
}

