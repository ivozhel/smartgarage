﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.CustomExceptions.CustomExceptions;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Services;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VisitServiceTests
{
    [TestClass]
    public class CheckoutVisit_Should
    {
        [TestMethod]
        public async Task CheckoutVisitCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("CheckoutVisitCorrectly");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapper);
                var result = await sut.CheckOutVisit(2);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task CheckoutVisitThrowWhenIncorrectStatus()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("CheckoutVisitThrowWhenIncorrectStatus");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapper);
                var result = await sut.CheckOutVisit(1);

                Assert.IsFalse(result);
            }
        }
    }
}
