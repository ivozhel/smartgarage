﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VisitServiceTests
{
    [TestClass]
    public class VisitUpdate_Should
    {
        [TestMethod]
        public async Task VisitUpdateCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VisitUpdateCorrectly");
            var model = new UpdateVisitDTO();
            model.Status = 2;

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapper);
                var result = await sut.UpdateAsync(1, model);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(model.Status, (int)actContext.Visits.FirstOrDefault(x => x.Id == 1).Status);
            }

        }

        [TestMethod]
        public async Task VisitUpdateWrongID()
        {

            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VisitUpdateWrongID");
            var model = new UpdateVisitDTO();
            model.Status = 2;


            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapper);
                var result = await sut.UpdateAsync(0, model);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
