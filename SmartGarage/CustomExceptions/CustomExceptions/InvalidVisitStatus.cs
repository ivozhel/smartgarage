﻿using System;

namespace SmartGarage.CustomExceptions.CustomExceptions
{
    public class InvalidVisitStatus : Exception
    {
        public InvalidVisitStatus()
            : base(string.Format("Visit status needs to be set to Ready(0)"))
        {

        }
    }
}
