﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Data.Models;
using SmartGarage.Data.Models.Enums;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using SmartGarage.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Controllers
{
    [Authorize()]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("[controller]")]
    public class VisitController : Controller
    {
        private readonly IVehicleModelService vehicleModelService;
        private readonly IManufacturerService manufacturerService;
        private readonly IVisitService visitService;
        private readonly IVehicleService vehicleService;
        private readonly ISvcService svcService;
        private readonly IUserService userService;
        private readonly UserManager<User> userManager;

        public VisitController(IVisitService visitService,
            IVehicleService vehicleService,
            IVehicleModelService vehicleModelService,
            IManufacturerService manufacturerService,
            UserManager<User> userManager, ISvcService svcService,
            IUserService userService)
        {
            this.vehicleModelService = vehicleModelService;
            this.manufacturerService = manufacturerService;
            this.visitService = visitService;
            this.vehicleService = vehicleService;
            this.userManager = userManager;
            this.svcService = svcService;
            this.userService = userService;
        }

        [Authorize()]
        [HttpGet]
        public async Task<IActionResult> Index(string isChecked, int pageNumber = 1, string currency = "EUR")
        {
            var pagerQueryObj = new PagerQueryObject(pageNumber, 0);

            if (User.IsInRole("Employee"))
            {
                return View(await visitService.GetAllAsync(new FilterVisitQuery(), pagerQueryObj, currency, isChecked));
            }

            return View(await visitService
                .GetAllAsync(new FilterVisitQuery(), pagerQueryObj, currency, isChecked, int.Parse(userManager.GetUserId(User))));
        }

        [Authorize()]
        [HttpGet("Details")]
        public async Task<IActionResult> Details(int id, int currencyChange)
        {
            var cur = (Currencies)currencyChange;
            var visit = await visitService.GetAsync(id, cur.ToString());

            visit.Currency = cur.ToString();

            if (visit == null)
            {
                return NotFound();
            }

            return View(visit);
        }

        [Authorize()]
        [HttpGet("ExchangeCurrency")]
        public async Task<IActionResult> ExchangeCurrency(int id, string currencyChange)
        {
            var visit = await visitService.GetAsync(id, currencyChange);

            visit.Currency = currencyChange;

            return PartialView("_VisitDetailsPartial", visit);
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Create")]
        public async Task<IActionResult> Create()
        {
            var model = new CreateVisitNotRegisterdCustomer()
            {
                Manufacturers = await manufacturerService.GetAllAync(),
                Models = await vehicleModelService.GetAllAsync(),
                Services = svcService.GetAllAsync().Result.ToList()
            };
            return View(model);
        }

        [Authorize(Roles = "Employee")]
        [HttpPost("Create")]
        public async Task<IActionResult> Create(CreateVisitNotRegisterdCustomer model)
        {
            var customer = new CreateUserDTO()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Phone = model.Phone
            };
            bool customerResult;
            try
            {
                customerResult = await userService.CreateCustomerAsync(customer);
            }
            catch
            {
                TempData["Messages"] = "User already exist";
                model.Manufacturers = await manufacturerService.GetAllAync();
                model.Models = await vehicleModelService.GetAllAsync();
                model.Services = svcService.GetAllAsync().Result.ToList();
                return View(model);
            }

            if (customerResult)
            {
                var newCustomer = await userManager.FindByEmailAsync(model.Email);
                try
                {
                    int modelId;
                    int makeId;
                    var modelResult = int.TryParse(model.VehicleModelId, out modelId);
                    var makeResult = int.TryParse(model.ManufacturerId, out makeId);

                    if (!makeResult)
                    {
                        var make = await manufacturerService.GetAync(model.ManufacturerId);
                        makeId = make.Id;
                    }

                    if (!modelResult)
                    {
                        var vehicleModel = await vehicleModelService.GetAsync(model.VehicleModelId);
                        modelId = vehicleModel.Id;
                    }


                    var vehicle = new CreateVehicleDTO()
                    {
                        ModelId = modelId,
                        ManufacturerId = makeId,
                        IdentityNumber = model.IdentityNumber,
                        PlateNumber = model.PlateNumber,
                        Type = model.Type,
                        CustomerId = newCustomer.Id

                    };

                    var vehicleResult = await vehicleService.CreateAsync(vehicle);

                    if (vehicleResult)
                    {
                        var visit = new CreateVisitDTO()
                        {
                            VehicleId = newCustomer.Vehicles.FirstOrDefault().Id
                        };
                        var servicesToAdd = model.Services.Where(x => x.AddToVisit).Select(x => x.Id);
                        var visitResult = await visitService.Create(visit);
                        if (visitResult != 0)
                        {
                            foreach (var item in servicesToAdd)
                            {
                                await visitService.AddServiceToVisit(visitResult, item);
                                continue;
                            }
                        }
                            TempData["Messages"] = "Successfully created visit";
                            return RedirectToAction(nameof(Index));
                    }

                }
                catch
                {
                    await userService.DeleteForRealAsync(newCustomer.Id);
                    TempData["Messages"] = "You need to choose model and make";
                    model.Manufacturers = await manufacturerService.GetAllAync();
                    model.Models = await vehicleModelService.GetAllAsync();
                    model.Services = svcService.GetAllAsync().Result.ToList();
                    return View(model);
                }
            }
            TempData["Messages"] = "There was a problem";
            return View();
        }



        [Authorize(Roles = "Employee")]
        [HttpGet("AddVisitToCustomer/{id}")]
        public async Task<IActionResult> AddVisitToCustomer(int id)
        {
            var customer = await userService.GetCustomerAsync(id);
            var mode = new AddVisitToCustomerViewModel()
            {
                CustomerName = customer.Name,
                Vehicles = customer.Vehicles.ToList(),
                Services = svcService.GetAllAsync().Result.ToList()
            };
            return View(mode);
        }

        [Authorize(Roles = "Employee")]
        [HttpPost("AddVisitToCustomer/{id}")]
        public async Task<IActionResult> AddVisitToCustomer(AddVisitToCustomerViewModel model, int id)
        {
            var newVisit = new CreateVisitDTO()
            {
                VehicleId = model.VehicleId
            };

            
            var visitTOcheck = await visitService.GetAllAsync();
            var result = visitTOcheck.Where(x => x.VehicleId == model.VehicleId).Any(x=>x.DateOut == "No out date assigned yet");
            if (result)
            {
                var customer = await userService.GetCustomerAsync(id);
                model.CustomerName = customer.Name;
                model.Vehicles = userService.GetCustomerAsync(id).Result.Vehicles.ToList();
                model.Services = svcService.GetAllAsync().Result.ToList();

                TempData["Messages"] = "Vehicle is in the shop already";
                return View(model);
            }
            var visitId = await visitService.Create(newVisit);
            var servicesToAdd = model.Services.Where(x => x.AddToVisit).Select(x => x.Id);

            if (visitId != 0)
            {
                foreach (var item in servicesToAdd)
                {
                    await visitService.AddServiceToVisit(visitId, item);
                    continue;
                }
            }
            TempData["Messages"] = "Successfully added visit";
            return RedirectToAction("Index", "Customers");
        }


        [Authorize(Roles = "Employee")]
        [HttpGet("EditServices/{id}")]
        public async Task<IActionResult> EditServices(int id)
        {
            var visit = await visitService.GetAsync(id);

            var model = new EditServicesInVisitViewModel
            {
                Services = visit.Services.ToList(),
                ServicesToAdd = svcService.GetAllAsync().Result.Where(x => !visit.Services.Any(y => x.Id == y.Id)).ToList()
            };

            if (visit == null)
            {
                return NotFound();
            }

            return View(model);
        }
        [Authorize(Roles = "Employee")]
        [HttpPost("EditServices/{id}")]
        public async Task<IActionResult> EditServices(EditServicesInVisitViewModel model, int id)
        {
            var visit = await visitService.GetAsync(id);
            List<bool> results = new List<bool>();

            if (visit == null)
            {
                return NotFound();
            }

            if (model.Services == null)
            {
                foreach (var item in model.ServicesToAdd.Where(x => x.AddToVisit))
                {
                    results.Add(await visitService.AddServiceToVisit(id, item.Id));
                }

                TempData["Messages"] = "Services added vehicle";

            }
            else
            {
                foreach (var item in model.Services.Where(x => x.AddToVisit))
                {
                    results.Add(await visitService.DeleteServiceFromVisit(id, item.Id));
                }


                TempData["Messages"] = "Services deleted vehicle";
            }
            if (results.Contains(false))
            {
                return BadRequest();
            }

            visit = await visitService.GetAsync(id);
            model.Services = visit.Services.ToList();
            model.ServicesToAdd = svcService.GetAllAsync().Result.Where(x => !visit.Services.Any(y => x.Id == y.Id)).ToList();

            return View(model);
        }
        [Authorize(Roles = "Employee")]
        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {

            var visit = await visitService.GetAsync(id);
            if (visit == null)
            {
                return NotFound();
            }

            return View(visit);

        }
        [Authorize(Roles = "Employee")]
        [HttpPost("Edit/{id}")]
        public async Task<IActionResult> Edit(VisitDTO model, int id)
        {
            var updateVisitDTO = new UpdateVisitDTO()
            {
                Status = model.Status
            };

            var result = await visitService.ChangeStatus(id, updateVisitDTO.Status);
            if (result)
            {
                TempData["Messages"] = "Successfully updated";
            }

            return RedirectToAction(nameof(Index));

        }

        [HttpGet("LoadPartial")]
        public async Task<IActionResult> LoadPartial(string isChecked,string search, DateTime from, DateTime to, bool dateFilter,
            int pageNumber = 1, string currency = "EUR")
        {
            var pagerQueryObj = new PagerQueryObject(pageNumber, 0);
            var filter = new FilterVisitQuery() { Vehicle = search, DateFrom = from, DateTo = to, DateFilter = dateFilter};
            if (User.IsInRole("Employee"))
            {
                return PartialView("_PartialViewVisitPager", await visitService.GetAllAsync(filter, pagerQueryObj, currency, isChecked));
            }
            return PartialView("_PartialViewVisitPager", await visitService
                .GetAllAsync(filter, pagerQueryObj, currency, isChecked, int.Parse(userManager.GetUserId(User))));
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("CheckOut/{id}")]
        public async Task<IActionResult> CheckOut(int id)
        {
            var visit = await visitService.GetAsync(id);

            if (visit == null)
            {
                return NotFound();
            }
            visit.Currency = "EUR";
            return View(visit);
        }

        [HttpPost("CheckOut/{id}"), ActionName("CheckOut")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CheckOutConfirmed(int id)
        {
            var result = await visitService.CheckOutVisit(id);
            if (result)
            {
                TempData["Messages"] = "Successfully checked out";
                return RedirectToAction(nameof(Index));
            }
            var visit = await visitService.GetAsync(id);

            if (visit == null)
            {
                return NotFound();
            }
            TempData["Messages"] = "Status needs to be ready";
            visit.Currency = "EUR";
            return View(visit);
        }
    }
}
