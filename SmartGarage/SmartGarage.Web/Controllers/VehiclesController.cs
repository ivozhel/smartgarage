﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Data.Models;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using SmartGarage.Web.Models;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Controllers
{
    [Authorize()]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("[controller]")]
    public class VehiclesController : Controller
    {
        private readonly IVehicleService vehicleService;
        private readonly IManufacturerService manufacturerService;
        private readonly IVehicleModelService vehicleModelService;
        private readonly IUserService userService;
        private readonly IModelMapper modelMapper;
        private readonly UserManager<User> userManager;

        public VehiclesController(IVehicleService vehicleService, IManufacturerService manufacturerService,
            IVehicleModelService vehicleModelService, IModelMapper modelMapper, IUserService userService,
            UserManager<User> userManager)
        {
            this.vehicleService = vehicleService;
            this.manufacturerService = manufacturerService;
            this.vehicleModelService = vehicleModelService;
            this.modelMapper = modelMapper;
            this.userService = userService;
            this.userManager = userManager;
        }

        [Authorize()]
        [HttpGet]
        public async Task<IActionResult> Index(string searchString, int pageNumber = 1)
        {
            var pagerQueryObj = new PagerQueryObject(pageNumber, 0);

            if (User.IsInRole("Employee"))
            {
                return View(await vehicleService.GetAllAsync(searchString, pagerQueryObj));
            }
            return View(await vehicleService
                .GetAllAsync(searchString, pagerQueryObj, int.Parse(userManager.GetUserId(User))));
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Create/{id}")]
        public async Task<IActionResult> Create(int id)
        {
            var customer = await userService.GetCustomerAsync(id);
            var model = new VehicleViewModel
            {
                Manufacturers = await manufacturerService.GetAllAync(),
                Models = await vehicleModelService.GetAllAsync(),
                CustomerName = customer.Name,
                CustomerId = id.ToString()
                
            };

            return View(model);
        }

        [Authorize(Roles = "Employee")]
        [HttpPost("Create/{id}")]
        public async Task<IActionResult> Create(VehicleViewModel model, int id)
        {
            
            try
            {
                int modelId;
                int makeId;
                var modelResult = int.TryParse(model.VehicleModelId, out modelId);
                var makeResult = int.TryParse(model.ManufacturerId, out makeId);

                if (!makeResult)
                {
                    var make = await manufacturerService.GetAync(model.ManufacturerId);
                    makeId = make.Id;
                }

                if (!modelResult)
                {
                    var vehicleModel = await vehicleModelService.GetAsync(model.VehicleModelId);
                    modelId = vehicleModel.Id;
                }

                var vehicle = new CreateVehicleDTO()
                {
                    CustomerId = id,
                    PlateNumber = model.PlateNumber,
                    IdentityNumber = model.IdentityNumber,
                    ManufacturerId = makeId,
                    ModelId = modelId,
                    Type = int.Parse(model.Type)
                };
                if (ModelState.IsValid)
                {
                    await vehicleService.CreateAsync(vehicle);
                    TempData["Messages"] = "Successfully created vehicle";
                    return RedirectToAction("Index", "Customers");
                }
                return View(model);
            }
            catch
            {
                model.CustomerName = userService.GetCustomerAsync(id).Result.Name;
                model.Manufacturers = await manufacturerService.GetAllAync();
                model.Models = await vehicleModelService.GetAllAsync();
                TempData["Messages"] = "Need to chose make and model";
                return View(model);
            }
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var vehicle = await vehicleService.GetAsync(id);
            var model = new VehicleViewModel
            {
                CustomerId = vehicle.CustomerId.ToString(),
                IdentityNumber = vehicle.IdentityNumber,
                PlateNumber = vehicle.PlateNumber,
                ManufacturerId = vehicle.ManufacturerId.ToString(),
                VehicleModelId = vehicle.ModelId.ToString(),
                Manufacturers = await manufacturerService.GetAllAync(),
                Models = await vehicleModelService.GetAllAsync(),
                CustomerName = vehicle.CustomerFullName,
                Customers = await userService.GetAllCustomersAsync()
            };

            return View(model);

        }

        [Authorize(Roles = "Employee")]
        [HttpPost("Edit/{id}")]
        public async Task<IActionResult> Edit(VehicleViewModel model, int id)
        {
            var vehicleToUpdate = new UpdateVehicleDTO()
            {
                IdentityNumber = model.IdentityNumber,
                PlateNumber = model.PlateNumber,
                CustomerId = int.Parse(model.CustomerId),
                ManufacturerId = int.Parse(model.ManufacturerId),
                ModelId = int.Parse(model.VehicleModelId),
                Type = int.Parse(model.Type)
            };

            var result = await vehicleService.UpdateAsync(vehicleToUpdate, id);
            if (result)
            {
                TempData["Messages"] = "Successfully updated";
            }

            return RedirectToAction(nameof(Index));

        }

        [Authorize()]
        [HttpGet("Details")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vehicle = await vehicleService.GetAsync(id);
            if (vehicle == null)
            {
                return NotFound();
            }

            return View(vehicle);
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var vehicle = await vehicleService.GetAsync(id);
            var model = new VehicleViewModel
            {
                IdentityNumber = vehicle.IdentityNumber,
                PlateNumber = vehicle.PlateNumber,
                CustomerName = vehicle.CustomerFullName,
            };
            if (vehicle == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost("Delete/{id}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var result = await vehicleService.DeleteAsync(id);
            if (result)
            {
                TempData["Messages"] = "Successfully deleted";
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet("RefreshManufacturers")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RefreshManufacturers(int? id)
        {
            var manufacturers = await manufacturerService.GetAllAync();

            return Json(manufacturers);
        }

        [HttpGet("Manufacturermodels/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Manufacturermodels(int? id)
        {
            var models = await vehicleModelService.GetAllAsync();

            return Json(models.Where(x => x.ManufacturerId == id).Select(x => modelMapper.ToVehicleModelWithId(x)));
        }
      
        [HttpGet("ManModels")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Manufacturermodels(string name)
        {
            int manId;
            var incoming = int.TryParse(name, out manId);
            var models = await vehicleModelService.GetAllAsync();
            if (incoming)
            {
                return Json(models.Where(x => x.ManufacturerId == manId).Select(x => modelMapper.ToVehicleModelWithId(x)));
            }

            return Json(models.Where(x => x.Manufacturer == name).Select(x => modelMapper.ToVehicleModelWithId(x)));
        }
    }
}
