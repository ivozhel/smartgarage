﻿using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using System.Collections.Generic;

namespace SmartGarage.Web.Models
{
    public class CustomerViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public IEnumerable<VehicleDTO> Vehicles { get; set; }
        public IEnumerable<VisitDTO> Visits { get; set; }
        public FilterUserQuery Filter { get; set; }
    }
}
