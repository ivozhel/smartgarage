﻿using SmartGarage.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Models
{
    public class AddVisitToCustomerViewModel
    {
        public string CustomerName { get; set; }
        public List<VehicleDTO> Vehicles { get; set; }
        public int VehicleId { get; set; }
        public List<ServiceDTO> Services { get; set; }
    }
}
