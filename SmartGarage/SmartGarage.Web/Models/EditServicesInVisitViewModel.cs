﻿using SmartGarage.Services.Models;
using System.Collections.Generic;

namespace SmartGarage.Web.Models
{
    public class EditServicesInVisitViewModel
    {
        public List<ServiceDTO> Services { get; set; }
        public List<ServiceDTO> ServicesToAdd { get; set; }
    }
}
