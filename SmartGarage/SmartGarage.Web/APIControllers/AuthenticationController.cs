﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Models;
using SmartGarage.Services.Services.Interfaces;
using System.Threading.Tasks;

namespace SmartGarage.Web.APIControllers
{
    public class AuthenticationController : Controller
    {
        private readonly IAuthenticateService authenticationService;
        public AuthenticationController(IAuthenticateService authenticationService)
        {
            this.authenticationService = authenticationService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] LoginDTO model)
        {
            var loginDTO = model;



            var user = await this.authenticationService.AuthenticateAsync(loginDTO);



            if (user != null)
            {
                return Ok(user);
            }



            return BadRequest(new { message = "Username or password is incorrect" });
        }
    }
}
