﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System.Threading.Tasks;


namespace SmartGarage.Web.APIControllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class ManufacturersController : ControllerBase
    {
        private readonly IManufacturerService manufacturerService;
        public ManufacturersController(IManufacturerService manufacturerService)
        {
            this.manufacturerService = manufacturerService;
        }

        [Authorize(Roles = "Employee")]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] int pageNumber, int pageSize)
        {
            var pager = new PagerQueryObject(pageNumber, pageSize);
            var result = await manufacturerService.GetAllAync(pager);
            if (result.Count != 0)
            {
                return Ok(result);
            }
            return NoContent();
        }
        [Authorize(Roles = "Employee")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await manufacturerService.GetAync(id);
            if (result != null)
            {
                return Ok(result);
            }
            return NotFound();
        }
        [Authorize(Roles = "Employee")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ManufacturerDTO mdodel)
        {
            var result = await manufacturerService.CreateAync(mdodel);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }
        [Authorize(Roles = "Employee")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ManufacturerDTO model)
        {
            var result = await manufacturerService.UpdateAsync(model, id);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }
        [Authorize(Roles = "Employee")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await manufacturerService.DeleteAsync(id);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }
    }
}
