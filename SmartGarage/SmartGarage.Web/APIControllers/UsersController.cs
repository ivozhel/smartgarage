﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System;
using System.Net;
using System.Threading.Tasks;

namespace SmartGarage.Web.APIControllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        //TODO: cannot catch appropriate exceptions, test the post method specifically
        [Authorize(Roles = "Employee")]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] FilterUserQuery userQuery, int pageNumber, int itemOnPage)
        {
            var pagerOptions = new PagerQueryObject(pageNumber, itemOnPage);
            return Ok(await userService.GetAllCustomersAsync(userQuery, pagerOptions));
        }
        [Authorize(Roles = "Employee")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var user = await userService.GetCustomerAsync(id);
            if (user == null)
            {
                return NoContent();
            }
            return Ok(user);
        }
        [Authorize(Roles = "Employee")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateUserDTO model)
        {
            try
            {
                return Ok(await userService.CreateCustomerAsync(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [Authorize(Roles = "Employee")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UpdateUserDTO model)
        {
            try
            {
                return Ok(await userService.UpdateAsync(model, id));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [Authorize(Roles = "Employee")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id >= 1)
            {
                var result = await this.userService.DeleteAsync(id);

                if (result)
                {
                    return Ok();
                }
            }

            return BadRequest("Failed to delete user due to invalid ID.");
        }
    }
}
