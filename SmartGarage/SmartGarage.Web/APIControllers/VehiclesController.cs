﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System.Threading.Tasks;

namespace SmartGarage.Web.APIControllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IVehicleService vehicleService;
        public VehiclesController(IVehicleService vehicleService)
        {
            this.vehicleService = vehicleService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string customerName, int pageNumber, int pageSize)
        {
            var pagerOptions = new PagerQueryObject(pageNumber, pageSize);
            return Ok(await vehicleService.GetAllAsync(customerName, pagerOptions));
        }
        [Authorize(Roles = "Employee")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var vehicle = await vehicleService.GetAsync(id);
            if (vehicle == null)
            {
                return NoContent();
            }
            return Ok(vehicle);
        }
        [Authorize(Roles = "Employee")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateVehicleDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            return Ok(await vehicleService.CreateAsync(model));
        }
        [Authorize(Roles = "Employee")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody] UpdateVehicleDTO model, int id)
        {
            if (model == null)
            {
                return BadRequest();
            }
            return Ok(await vehicleService.UpdateAsync(model, id));
        }
        [Authorize(Roles = "Employee")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await vehicleService.DeleteAsync(id);

            if (result)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }


    }
}
