﻿
    $(document).ready(function () {
            $('#order').hide();
            $('#filterBy').change(function () {
                if ($('#filterBy').val() == "def") {

                    $('#order').hide();
                }
                else {
                    $('#order').show();
                }
            });
    });

        $(document).ready(function () {

            var page = $('#pageNum').val();

            $("#order").on("change", function () {              
                $.ajax({
                    url: "/Customers/PartialPager",
                    type: "GET",
                    data: {
                        customerName: $('#customerName').val(),
                        email: $('#customerEmail').val(),
                        phone: $('#customerPhone').val(),
                        vehicle: $('#customerVehicle').val(),
                        from: $('#dateFrom').val(),
                        to: $('#dateTo').val(),
                        dateFilter: $('#dateFilter').val(),
                        filterBy: $('#filterBy').val(),
                        order: $('#order').val()
                    }
                })
                    .done(function (partialViewResult) {
                        $("#pagerCustomer").html(partialViewResult);
                    });

            });

            $(document).on("click", "#search", function () {
                $.ajax({
                    url: "/Customers/PartialPager",
                    type: "GET",
                    data: {
                        customerName: $('#customerName').val(),
                        email: $('#customerEmail').val(),
                        phone: $('#customerPhone').val(),
                        vehicle: $('#customerVehicle').val(),
                        from: $('#dateFrom').val(),
                        to: $('#dateTo').val(),
                        dateFilter: $('#dateFilter').val(),
                        filterBy: $('#filterBy').val(),
                        order: $('#order').val()
                    }
                })
                    .done(function (partialViewResult) {
                        $("#pagerCustomer").html(partialViewResult);
                    });

            });

            $("#filterBy").on("change", function () {
                $.ajax({
                    url: "/Customers/PartialPager",
                    type: "GET",
                    data: {
                        customerName: $('#customerName').val(),
                        email: $('#customerEmail').val(),
                        phone: $('#customerPhone').val(),
                        vehicle: $('#customerVehicle').val(),
                        from: $('#dateFrom').val(),
                        to: $('#dateTo').val(),
                        dateFilter: $('#dateFilter').val(),
                        filterBy: $('#filterBy').val(),
                        order: $('#order').val()
                    }
                })
                    .done(function (partialViewResult) {
                        $("#pagerCustomer").html(partialViewResult);
                    });

            });

            $(document).on("click", "#next", function () {
                $.ajax({
                    url: "/Customers/PartialPager",
                    type: "GET",
                    data: {
                        customerName: $('#customerName').val(),
                        email: $('#customerEmail').val(),
                        phone: $('#customerPhone').val(),
                        vehicle: $('#customerVehicle').val(),
                        from: $('#dateFrom').val(),
                        to: $('#dateTo').val(),
                        dateFilter: $('#dateFilter').val(),
                        filterBy: $('#filterBy').val(),
                        order: $('#order').val(),
                        pageNumber: parseInt(page) + 1
                    }
                })
                    .done(function (partialViewResult) {
                        $("#pagerCustomer").html(partialViewResult);
                    });
            });

            $(document).on("click", "#prev", function () {
                $.ajax({
                    url: "/Customers/PartialPager",
                    type: "GET",
                    data: {
                        customerName: $('#customerName').val(),
                        email: $('#customerEmail').val(),
                        phone: $('#customerPhone').val(),
                        vehicle: $('#customerVehicle').val(),
                        from: $('#dateFrom').val(),
                        to: $('#dateTo').val(),
                        dateFilter: $('#dateFilter').val(),
                        filterBy: $('#filterBy').val(),
                        order: $('#order').val(),
                        pageNumber: parseInt(page) - 1,
                    }
                })
                    .done(function (partialViewResult) {
                        $("#pagerCustomer").html(partialViewResult);
                    });
            });
        });