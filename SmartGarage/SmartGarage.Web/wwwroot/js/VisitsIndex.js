﻿//Visits Index
$(document).ready(function () {
    $("#checked").on("change", function () {
        var val = $('#checked').val();
        $.ajax({
            url: "/Visit/LoadPartial",
            type: "GET",
            data: {
                isChecked: val,
                search: $('#customerVehicle').val(),
                to: $('#dateTo').val(),
                from: $('#dateFrom').val(),
                dateFilter: $('#dateFilter').val()
            }
        })
            .done(function (partialViewResult) {
                $("#partialView").html(partialViewResult);
            });

    });

    $(document).on("click", "#search", function () {
        var val = $('#checked').val();
        $.ajax({
            url: "/Visit/LoadPartial",
            type: "GET",
            data: {
                isChecked: val,
                search: $('#customerVehicle').val(),
                to: $('#dateTo').val(),
                from: $('#dateFrom').val(),
                dateFilter: $('#dateFilter').val()
            }
        })
            .done(function (partialViewResult) {
                $("#partialView").html(partialViewResult);
            });
    });

    $(document).on("click", "#next", function () {
        var val = $('#checked').val();
        var page = $('#pageNum').val();
        $.ajax({
            url: "/Visit/LoadPartial",
            type: "GET",
            data: {
                isChecked: val,
                pageNumber: parseInt(page) + 1,
                to: $('#dateTo').val(),
                from: $('#dateFrom').val(),
                search: $('#search').val(),
                dateFilter: $('#dateFilter').val()
            }
        })
            .done(function (partialViewResult) {
                $("#partialView").html(partialViewResult);
            });
    });
    $(document).on("click", "#prev", function () {
        var val = $('#checked').val();
        var page = $('#pageNum').val();
        $.ajax({
            url: "/Visit/LoadPartial",
            type: "GET",
            data: {
                isChecked: val,
                pageNumber: parseInt(page) - 1,
                search: $('#search').val(),
                to: $('#dateTo').val(),
                from: $('#dateFrom').val(),
                dateFilter: $('#dateFilter').val()
            }
        })
            .done(function (partialViewResult) {
                $("#partialView").html(partialViewResult);
            });
    });
});
//